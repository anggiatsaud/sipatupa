<?php

namespace backend\controllers;

use Yii;
use backend\models\Restoran;
use backend\models\RestoranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Fasilitas;
use backend\models\FasilitasSearch;
use backend\models\Makanan;
use backend\models\MakananSearch;
use backend\models\Minuman;
use backend\models\MinumanSearch;


/**
 * RestoranController implements the CRUD actions for Restoran model.
 */
class RestoranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Restoran models.
     * @return mixed
     */
   public function actionIndex()
    {
        $searchModel = new RestoranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Restoran();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

      public function actionDaftarrestoran()
    {
        $searchModel = new RestoranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Restoran();

        return $this->render('daftarhotel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Restoran model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Restoran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Restoran();

        $dataRestoran = \yii\helpers\ArrayHelper::map(Restoran::find()->asArray()->all(), 'id', 'nama');

        if ($model->load(Yii::$app->request->post())) {
            $restoran = $model->nama;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataRestoran' => $dataRestoran,
            ]);
        }
    }

    /**
     * Updates an existing Restoran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $dataRestoran = \yii\helpers\ArrayHelper::map(Restoran::find()->asArray()->all(), 'id', 'nama');

        if ($model->load(Yii::$app->request->post())) {
            $restoran = $model->nama;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataRestoran' => $dataRestoran,
            ]);
        }
    }

    /**
     * Deletes an existing Restoran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Restoran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Restoran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Restoran::findOne($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndexrestoran($id)
    {
        $searchModelFas = new FasilitasSearch();
        $dataProviderFas = $searchModelFas->search(Yii::$app->request->queryParams);
        $searchModelMa = new MakananSearch();
        $dataProviderMa = $searchModelMa->search(Yii::$app->request->queryParams);
        $searchModelMi = new MinumanSearch();
        $dataProviderMi = $searchModelMi->search(Yii::$app->request->queryParams);
        
        $restoran = Restoran::findOne($id);
        return $this->render('indexrestoran',[
            'restoran' => $restoran,
            'model' => $this->findModelRestoran($id),

            'searchModelFas' => $searchModelFas,
            'dataProviderFas' => $dataProviderFas,
            'searchModelMa' => $searchModelMa,
            'dataProviderMa' => $dataProviderMa,
            'searchModelMi' => $searchModelMi,
            'dataProviderMi' => $dataProviderMi,
        ]);
    }

    protected function findModelRestoran($id)
    {
        if(($model = Restoran::find()->where('id = :id',[':id' => $id])->one()) !== null){
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}