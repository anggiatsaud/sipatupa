<?php

namespace backend\controllers;

use Yii;
use backend\models\Kamarfron;
use backend\models\KamarfronSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\JenisKamarfron;
use backend\models\Transaksifron;
use common\models\Pelanggan;
use backend\models\DetailTransaksifron;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * KamarfronController implements the CRUD actions for Kamarfron model.
 */
class KamarfronController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kamarfron models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KamarfronSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query =  new \yii\db\Query;
        $query->select(['*'])  
        ->from('kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->groupBy('jenis_kamar.tipe_kamar');

        $command = $query->createCommand();
        $kamars = $command->queryAll();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kamars' =>$kamars,
        ]);
    }

    /**
     * Displays a single Kamarfron model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
       $bookingForm = new Transaksifron();
        $query =  new \yii\db\Query;
        $query->select(['*'])  
        ->from('kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->groupBy('jenis_kamar.tipe_kamar');
        $query->where([
            'jenis_kamar.id' => $id,
            ]);

        $count = (new \yii\db\Query())
        ->from('kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->where(['jenis_kamar.id' => $id])
        ->count();


        $command = $query->createCommand();
        $model = $command->queryOne();

        return $this->render('view', [
            'model' => $model,
            'bookingForm' => $bookingForm,
            'jumlah_kamar' => $count,
        ]);
    }

    /**
     * Creates a new Kamarfron model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kamarfron();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kamar]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Kamarfron model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kamar]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Kamarfron model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kamarfron model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kamarfron the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kamarfron::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
