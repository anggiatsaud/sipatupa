<?php

namespace backend\controllers;

use Yii;
use backend\models\Minuman;
use backend\models\MinumanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * MinumanController implements the CRUD actions for Minuman model.
 */
class MinumanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Minuman models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MinumanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Minuman();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDaftarminuman()
    {
        $searchModel = new MinumanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Minuman();

        return $this->render('daftarminuman', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Minuman model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Minuman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Minuman();

        $dataMinuman = \yii\helpers\ArrayHelper::map(Minuman::find()->asArray()->all(), 'id', 'nama');

        if ($model->load(Yii::$app->request->post())) {
            $minuman = $model->nama;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataMinuman' => $dataMinuman,
            ]);
        }
    }


    /**
     * Updates an existing Sekolah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $dataMinuman = \yii\helpers\ArrayHelper::map(Minuman::find()->asArray()->all(), 'id', 'nama');

        if ($model->load(Yii::$app->request->post())) {
            $minuman = $model->nama;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataMinuman' => $dataMinuman,
            ]);
        }
    }

    /**
     * Finds the Minuman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Minuman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
   protected function findModel($id)
    {
        if (($model = Minuman::findOne($id)) !== null) {
            return $model;
        } else {

             throw new NotFoundHttpException('The requested page does not exist.');
             }
        }
    public function actionIndexminuman($id)
    {
        $minuman = Minuman::findOne($id);
        return $this->render('indexminuman',[
            'minuman' => $minuman,
            'model' => $this->findModelMinuman($id),
        ]);
    }

    protected function findModelMinuman($id)
    {
        if(($model = Minuman::find()->where('id = :id',[':id' => $id])->one()) !== null){
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
