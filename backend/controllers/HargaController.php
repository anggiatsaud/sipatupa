<?php

namespace backend\controllers;

use Yii;
use backend\models\Harga;
use backend\models\HargaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\JenisKamar;
use yii\web\ForbiddenHttpException;
/**
 * HargaController implements the CRUD actions for Harga model.
 */
class HargaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Harga models.
     * @return mixed
     */
    public function actionIndex()
    {
        

        $events = Harga::find()->all();

        $tasks = array();
       foreach ($events as $eve) {
          $event1= new \yii2fullcalendar\models\Event();
          $event1->id = $eve->id;
          $event1->start = $eve->dari;
          $event1->end = $eve->sampai;
          $event1->description = $eve->harga;
          $event1->title = $eve->harga;
          
        //die($eve->created_date);
          if($eve->sampai != '0000-00-00'){
                $event1->backgroundColor = '#3cc441';
                $event1->borderColor = '#3cc441';
                $event1->end = $eve->sampai;
          }
          else{
                $event1->backgroundColor = '#3a87ad';
                $event1->borderColor = '#3a87ad';
          }
          $tasks[] = $event1;
       }

        return $this->render('index', [
           'events' => $tasks,
        ]);
    }

    /**
     * Displays a single Harga model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Harga model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('create-harga'))
        {
            $model = new Harga();
            $jenis_kamar = JenisKamar::find()->all(); 
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'jenis_kamar' => $jenis_kamar,
                ]);
            }
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Harga model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('update-harga'))
        {
            $model = $this->findModel($id);
            $jenis_kamar = JenisKamar::find()->all();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                    'jenis_kamar' => $jenis_kamar,
                ]);
            }            
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Deletes an existing Harga model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('delete-harga'))
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);            
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Harga model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Harga the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Harga::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
