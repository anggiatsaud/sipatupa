<?php

namespace backend\controllers;

use Yii;
use backend\models\Hotel;
use backend\models\HotelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use backend\models\Fasilitas;
use backend\models\FasilitasSearch;
use backend\models\JenisKamar;
use backend\models\JenisKamarSearch;


/**
 * HotelController implements the CRUD actions for Hotel model.
 */
class HotelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hotel models.
     * @return mixed
     */
   public function actionIndex()
    {
        $searchModel = new HotelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      

        $model = new Hotel();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

      public function actionDaftarhotel()
    {
        $searchModel = new HotelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Hotel();

        return $this->render('daftarhotel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }
    /**
     * Displays a single Hotel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hotel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   public function actionCreate()
    {

        $model = new Hotel();

        $dataHotel = \yii\helpers\ArrayHelper::map(Hotel::find()->asArray()->all(), 'id', 'nama_hotel');

        if ($model->load(Yii::$app->request->post())) {
            $hotel = $model->nama_hotel;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataHotel' => $dataHotel,
            ]);
        }
    }


    /**
     * Updates an existing Hotel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $dataHotel = \yii\helpers\ArrayHelper::map(Hotel::find()->asArray()->all(), 'id', 'nama_hotel');

        if ($model->load(Yii::$app->request->post())) {
            $hotel = $model->nama_hotel;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataHotel' => $dataHotel,
            ]);
        }
    }


    /**
     * Deletes an existing Hotel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hotel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hotel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hotel::findOne($id)) !== null) {
            return $model;
        } else {

             throw new NotFoundHttpException('The requested page does not exist.');
             }
        }
    public function actionIndexhotel($id)
    {
        $searchModelFas = new FasilitasSearch();
        $dataProviderFas = $searchModelFas->search(Yii::$app->request->queryParams);
         $searchModelJe = new JenisKamarSearch();
        $dataProviderJe = $searchModelJe->search(Yii::$app->request->queryParams);

        $hotel = Hotel::findOne($id);
        return $this->render('indexhotel',[
            'hotel' => $hotel,
            'model' => $this->findModelHotel($id),
            'searchModelFas' => $searchModelFas,
            'dataProviderFas' => $dataProviderFas,
            'searchModelJe' => $searchModelJe,
            'dataProviderJe' => $dataProviderJe,

        ]);
    }

    protected function findModelHotel($id)
    {
        if(($model = Hotel::find()->where('id = :id',[':id' => $id])->one()) !== null){
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
