<?php

namespace backend\controllers;

use Yii;
use backend\models\Transaksi;
use backend\models\TransaksiSearch;
use kartik\date\DatePicker;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use backend\models\Pembayaran;
use yii\web\ForbiddenHttpException;

/**
 * TransaksiController implements the CRUD actions for Transaksi model.
 */
class TransaksiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transaksi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Transaksi();
        $count = (new \yii\db\Query())
        ->from('transaksi')
        ->innerJoin('pembayaran', 'transaksi.id_transaksi = pembayaran.id_transaksi')
        ->count(); 

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT t.id_transaksi FROM transaksi t INNER JOIN pembayaran p on t.id_transaksi = p.id_transaksi 
            INNER JOIN pelanggan c on t.id_pelanggan = c.id_pelanggan ORDER BY t.id_transaksi DESC',
            'pagination' => [
                'pageSize' => 20,
                'totalCount'  => $count,
            ],
            'sort' => [
                'attributes' => [
                    'id_pembayaran',
                ],
            ],
        ]);


        // print_r($dataProvider->getModels());
        // die(); 

        $sql = "SELECT * FROM transaksi WHERE DATEDIFF(CURDATE(),transaksi.tanggal_booking) > 4 AND transaksi.status = 'menunggu pembayaran'";
        $rawData = Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($rawData as $transaksi) {
                $sqlDetail = "SELECT * FROM detail_transaksi WHERE id_transaksi = ".$transaksi['id_transaksi'];
                $rawDataDetail = Yii::$app->db->createCommand($sqlDetail)->queryAll();
                foreach ($rawDataDetail as $key => $detailTransaksi) {  
                    $connectionDetail = \Yii::$app->db;
                    $commandDetail = $connectionDetail->createCommand("DELETE FROM transaksi WHERE id_transaksi=".$transaksi['id_transaksi']);
                    $commandDetail->execute();                                      
                } 
                $connectionTransaksi = \Yii::$app->db;
                $commandTransaksi = $connectionTransaksi->createCommand("DELETE FROM transaksi WHERE id_transaksi=".$transaksi['id_transaksi']);
                $commandTransaksi->execute(); 
            }          

        $models = $dataProvider->getModels();

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearch()
    {
        $model = new Transaksi();


        if ($model->load(Yii::$app->request->post())) {
            $dataProvider = new SqlDataProvider([
            'sql' => "SELECT * FROM transaksi t INNER JOIN pembayaran p on t.id_transaksi = p.id_transaksi INNER JOIN pelanggan c on t.id_pelanggan = c.id_pelanggan WHERE t.kode_transaksi = 'SIHSNfc73483cfe'",
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'attributes' => [
                        'id_pembayaran',
                    ],
                ],
            ]);          

            return $this->render('index', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            $this->actionIndex();           
        }
    }
    /**
     * Displays a single Transaksi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $pembayaran = Pembayaran::find()->where(['id_transaksi' => $id]);
        $dataProvider = new ActiveDataProvider(
            [
                'query'=>$pembayaran,
                'pagination'=>[
                    'pageSize'=>10
                ]
            ]
        );

        $dataProvider1 = new SqlDataProvider([
            'sql' => "SELECT * from kamar k INNER JOIN jenis_kamar j ON k.id_jenis_kamar = j.id
                      INNER JOIN detail_transaksi d on d.id_kamar = k.id_kamar where d.id_transaksi = $id",
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'no_kamar',
                ],
            ],
        ]);

        $query =  new \yii\db\Query;
        $query->select(['*'])  
        ->from('pembayaran')
        ->where(['id_transaksi' => $id]);

        $command = $query->createCommand();
        $pembayaran = $command->queryOne();      

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider'=>$dataProvider,
            'dataProvider1' => $dataProvider1,
            'pembayaran' => $pembayaran,
        ]);
    }

    /**
     * Creates a new Transaksi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Transaksi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_transaksi]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionEmail($id_transaksi)
    {
        if(Yii::$app->user->can('kirim-tiket')){
            $date = date('H:i');
            echo $date;
            $sapa = '';
            if ($date < 12)
                $sapa = "Pagi";
            else if ($date < 14)
                $sapa = 'Siang';
            else if ($date < 18)
                $sapa = 'Sore';

            $query =  new \yii\db\Query;
            $query->select(['*'])  
            ->from('pelanggan')
            ->innerJoin('transaksi', 'pelanggan.id_pelanggan = transaksi.id_pelanggan')
            ->where(['transaksi.id_transaksi' => $id_transaksi]);

            $command = $query->createCommand();
            $model = $command->queryOne();        

            $message = 'Pemberitahuan Pembayaran Berhasil<br>'
                    . 'Selamat ' . $sapa .' '.$model['nama']. '<br>'
                    .' Kami dari Hotel Sere Nauli hendak memberitahukan bahwa pembayaran yang anda lakukan berhasil'
                    . ' <br> <br>Atas perhatian Bapak/Ibu Sekalian, Kami Ucapkan Terimakasih'
                    . '<br><br><br> <hr> Dikirim oleh : <br>'
                    . '<b>Sistem Informasi Hotel Sere Nauli<b>';

            $tujuan = $model['email'];    
            
            $path = "C:\Users\Okta\Downloads\SIHSN$id_transaksi.pdf";

            $email = Yii::$app->mailer->compose()
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'SIHSN'])
                    ->setTo($tujuan)
                    ->setSubject("Notifikasi Pembayaran diterima")
                    ->setHtmlBody($message)
                    ->attach($path)
                    ->send();

            $connection = \Yii::$app->db;
            $command = $connection->createCommand("UPDATE transaksi SET status_struk='sudah dikirim' WHERE id_transaksi=$id_transaksi");
            $command->execute();                
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing Transaksi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_transaksi]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Transaksi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCheckin($id)
    {
        if(Yii::$app->user->can('check-in')){
            $connection = \Yii::$app->db;
            $command = $connection->createCommand("UPDATE transaksi SET status='check in' WHERE id_transaksi=$id");
            $command->execute();                 
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }
    }    

    public function actionCheckout($id)
    {
        if(Yii::$app->user->can('check-out')){
            $connection = \Yii::$app->db;        
            $command = $connection->createCommand("UPDATE transaksi SET status='check out' WHERE id_transaksi=$id");
            $command->execute();           
            return $this->redirect(['index']);
        }else{
            throw new ForbiddenHttpException;
        }
    }  

    public function actionChart()
    {
        if(Yii::$app->user->can ('melihat-laporan'))
        {
            $model = new Transaksi();
            if($model->load(Yii::$app->request->post()))
            {
                $sql = "SELECT  *,SUM(t.durasi) as banyak_bookingan FROM transaksi t 
                        INNER JOIN detail_transaksi d on t.id_transaksi = d.id_transaksi
                        INNER JOIN kamar k on k.id_kamar = d.id_kamar 
                        INNER JOIN jenis_kamar j on j.id = k.id_jenis_kamar
                        WHERE t.tanggal_check_in BETWEEN '$model->tanggal_check_in' AND '$model->tanggal_check_out'
                        GROUP BY j.id";
                $rawData = Yii::$app->db->createCommand($sql)->queryAll();

                $sql1 = "SELECT  *,SUM(transaksi.jumlah_transaksi) as tunai,MONTH(transaksi.tanggal_pelunasan) as bulan
                        FROM transaksi 
                        WHERE transaksi.tanggal_pelunasan BETWEEN '$model->tanggal_check_in' AND '$model->tanggal_check_out'
                        GROUP BY MONTH(transaksi.tanggal_pelunasan)";
                $rawDataTunai = Yii::$app->db->createCommand($sql1)->queryAll();

                $sql2 = "SELECT *,SUM(pembayaran.total_transaksi) as transfer, MONTH(pembayaran.tanggal_konfirmasi) as bulan
                        FROM pembayaran
                        WHERE pembayaran.tanggal_konfirmasi BETWEEN '$model->tanggal_check_in' AND '$model->tanggal_check_out'
                        GROUP BY MONTH(pembayaran.tanggal_konfirmasi)";
                $rawDataTransfer = Yii::$app->db->createCommand($sql2)->queryAll();  

                $dataProvider = new SqlDataProvider([
                    'sql' => "SELECT jenis_kamar.tipe_kamar,jenis_kamar.harga,jenis_kamar.muatan,COUNT(*) as jumlahKamar
                            FROM jenis_kamar INNER JOIN kamar ON jenis_kamar.id = kamar.id_jenis_kamar 
                            GROUP BY jenis_kamar.tipe_kamar 
                            ORDER BY jumlahKamar DESC ",
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort' => [
                        'attributes' => [
                            'id_pembayaran',
                        ],
                    ],
                ]);                          
                
                return $this->render('chart',[
                    'model' => $model,
                    'rawData' => $rawData,
                    'rawDataTunai' => $rawDataTunai,
                    'rawDataTransfer' => $rawDataTransfer,
                    'dataProvider' => $dataProvider,
                ]); 
            }else{
                $sql = "SELECT  *,SUM(t.durasi) as banyak_bookingan FROM transaksi t 
                        INNER JOIN detail_transaksi d on t.id_transaksi = d.id_transaksi
                        INNER JOIN kamar k on k.id_kamar = d.id_kamar 
                        INNER JOIN jenis_kamar j on j.id = k.id_jenis_kamar
                        WHERE t.tanggal_check_in BETWEEN '$model->tanggal_check_in' AND '$model->tanggal_check_out'
                        GROUP BY j.id";
                $rawData = Yii::$app->db->createCommand($sql)->queryAll();

                $sql1 = "SELECT  *,SUM(transaksi.jumlah_transaksi) as tunai,MONTH(transaksi.tanggal_pelunasan) as bulan
                        FROM transaksi 
                        WHERE transaksi.tanggal_pelunasan BETWEEN '$model->tanggal_check_in' AND '$model->tanggal_check_out'
                        GROUP BY MONTH(transaksi.tanggal_pelunasan)";
                $rawDataTunai = Yii::$app->db->createCommand($sql1)->queryAll();

                $sql2 = "SELECT *,SUM(pembayaran.total_transaksi) as transfer, MONTH(pembayaran.tanggal_konfirmasi) as bulan
                        FROM pembayaran
                        WHERE pembayaran.tanggal_konfirmasi BETWEEN '$model->tanggal_check_in' AND '$model->tanggal_check_out'
                        GROUP BY MONTH(pembayaran.tanggal_konfirmasi)";
                $rawDataTransfer = Yii::$app->db->createCommand($sql2)->queryAll();     

                $dataProvider = new SqlDataProvider([
                    'sql' => "SELECT jenis_kamar.tipe_kamar,jenis_kamar.harga,jenis_kamar.muatan,COUNT(*) as jumlahKamar 
                            FROM jenis_kamar INNER JOIN kamar ON jenis_kamar.id = kamar.id_jenis_kamar 
                            GROUP BY jenis_kamar.tipe_kamar 
                            ORDER BY jumlahKamar DESC ",
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort' => [
                        'attributes' => [
                            'id_pembayaran',
                        ],
                    ],
                ]);                        
                
                return $this->render('chart',[
                    'model' => $model,
                    'rawData' => $rawData,
                    'rawDataTunai' => $rawDataTunai,
                    'rawDataTransfer' => $rawDataTransfer,
                    'dataProvider' => $dataProvider,
                ]);             
            }   
        }else{
            throw new ForbiddenHttpException;
        }
    }

    /**
     * Finds the Transaksi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaksi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaksi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
