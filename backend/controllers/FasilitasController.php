<?php

namespace backend\controllers;

use Yii;
use backend\models\Fasilitas;
use backend\models\FasilitasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * FasilitasController implements the CRUD actions for Fasilitas model.
 */
class FasilitasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fasilitas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FasilitasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $model = new Fasilitas();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDaftarfasilitas()
    {
        $searchModel = new FasilitasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Fasilitas();

        return $this->render('daftarfasilitas', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Fasilitas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fasilitas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fasilitas();

        $dataFasilitas = \yii\helpers\ArrayHelper::map(Fasilitas::find()->asArray()->all(), 'id', 'nama_fasilitas');

        if ($model->load(Yii::$app->request->post())) {
            $fasilitas = $model->nama_fasilitas;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataFasilitas' => $dataFasilitas,
            ]);
        }
    }

    /**
     * Updates an existing Fasilitas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $dataFasilitas = \yii\helpers\ArrayHelper::map(Fasilitas::find()->asArray()->all(), 'id', 'nama_fasilitas');

        if ($model->load(Yii::$app->request->post())) {
            $fasilitas = $model->nama_fasilitas;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataFasilitas' => $dataFasilitas,
            ]);
        }
    }


    /**
     * Deletes an existing Fasilitas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fasilitas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fasilitas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fasilitas::findOne($id)) !== null) {
            return $model;
        } else {

             throw new NotFoundHttpException('The requested page does not exist.');
             }
        }
    public function actionIndexfasilitas($id)
    {
        $fasilitas = Fasilitas::findOne($id);
        return $this->render('indexfasilitas',[
            'fasilitas' => $fasilitas,
            'model' => $this->findModelFasilitas($id),
        ]);
    }

    protected function findModelFasilitas($id)
    {
        if(($model = Fasilitas::find()->where('id = :id',[':id' => $id])->one()) !== null){
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
