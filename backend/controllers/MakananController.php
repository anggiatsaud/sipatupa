<?php

namespace backend\controllers;

use Yii;
use backend\models\Makanan;
use backend\models\MakananSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;


/**
 * MakananController implements the CRUD actions for Makanan model.
 */
class MakananController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Makanan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MakananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Makanan();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDaftarmakanan()
    {
        $searchModel = new MakananSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Makanan();

        return $this->render('daftarmakanan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Makanan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Makanan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Makanan();

        $dataMakanan = \yii\helpers\ArrayHelper::map(Makanan::find()->asArray()->all(), 'id', 'nama');

        if ($model->load(Yii::$app->request->post())) {
            $makanan = $model->nama;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataMakanan' => $dataMakanan,
            ]);
        }
    }

    /**
     * Updates an existing Makanan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
       $model = $this->findModel($id);

       $dataMakanan = \yii\helpers\ArrayHelper::map(Makanan::find()->asArray()->all(), 'id', 'nama');

        if ($model->load(Yii::$app->request->post())) {
            $makanan = $model->nama;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataMakanan' => $dataMakanan,
            ]);
        }
    }
    /**
     * Deletes an existing Makanan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Makanan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Makanan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Makanan::findOne($id)) !== null) {
            return $model;
        } else {

             throw new NotFoundHttpException('The requested page does not exist.');
             }
        }
    public function actionIndexmakanan($id)
    {
        $makanan = Makanan::findOne($id);
        return $this->render('indexmakanan',[
            'makanan' => $makanan,
            'model' => $this->findModelMakanan($id),
        ]);
    }

    protected function findModelMakanan($id)
    {
        if(($model = Makanan::find()->where('id = :id',[':id' => $id])->one()) !== null){
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
