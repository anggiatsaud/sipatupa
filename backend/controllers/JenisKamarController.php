<?php

namespace backend\controllers;

use Yii;
use backend\models\JenisKamar;
use backend\models\JenisKamarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * JenisKamarController implements the CRUD actions for JenisKamar model.
 */
class JenisKamarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all JenisKamar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JenisKamarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new JenisKamar();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionDaftarjeniskamar()
    {
        $searchModel = new JenisKamarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Fasilitas();

        return $this->render('daftarjeniskamar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single JenisKamar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JenisKamar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JenisKamar();

        $dataJenisKamar = \yii\helpers\ArrayHelper::map(JenisKamar::find()->asArray()->all(), 'id', 'tipe_kamar');

        if ($model->load(Yii::$app->request->post())) {
            $jeniskamar = $model->tipe_kamar;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataJenisKamar' => $dataJenisKamar,
            ]);
        }
    }
    /**
     * Updates an existing JenisKamar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       $dataJenisKamar = \yii\helpers\ArrayHelper::map(JenisKamar::find()->asArray()->all(), 'id', 'tipe_kamar');

        if ($model->load(Yii::$app->request->post())) {
            $jeniskamar = $model->tipe_kamar;
            $gambar = UploadedFile::getInstance($model, 'gambar');
            $model->gambar = $gambar->baseName. '.'.$gambar->extension;

            if($model->save()){
                $gambar->saveAs('uploads/'.$model->gambar);

                return $this->redirect(['view','id' =>$model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'dataJenisKamar' => $dataJenisKamar,
            ]);
        }
    }
    /**
     * Deletes an existing JenisKamar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JenisKamar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JenisKamar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
  protected function findModel($id)
    {
        if (($model = JenisKamar::findOne($id)) !== null) {
            return $model;
        } else {

             throw new NotFoundHttpException('The requested page does not exist.');
             }
        }
    public function actionIndexjeniskamar($id)
    {
        $jeniskamar = JenisKamar::findOne($id);
        return $this->render('indexjeniskamar',[
            'jenis-kamar' => $jeniskamar,
            'model' => $this->findModelJenisKamar($id),
        ]);
    }

    protected function findModelJenisKamar($id)
    {
        if(($model = JenisKamar::find()->where('id = :id',[':id' => $id])->one()) !== null){
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
