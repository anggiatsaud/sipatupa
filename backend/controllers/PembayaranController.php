<?php

namespace backend\controllers;

use Yii;
use backend\models\Pembayaran;
use backend\models\PembayaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Transaksi;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\Pekerja;
use common\models\Pelanggan;
use kartik\mpdf\pdf;
use yii\web\ForbiddenHttpException;

/**
 * PembayaranController implements the CRUD actions for Pembayaran model.
 */
class PembayaranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pembayaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PembayaranSearch();
        $query = Pembayaran::find();        
        $provider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id_pembayaran' => SORT_DESC
                ]
            ],            
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);          

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Displays a single Pembayaran model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pembayaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pembayaran();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pembayaran]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pembayaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pembayaran]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionLunasiPembayaran($id_transaksi,$id_pembayaran)
    {
         if(Yii::$app->user->can('lunasi'))
        {
        $model_transaksi = Transaksi::find()
                           ->where(['id_transaksi' => $id_transaksi])
                           ->one();

        $user_id = Yii::$app->user->id;
        $pekerja = Pekerja::findOne(['account_id' => $user_id]);  
        $id_staff = $pekerja['id_staff'];

        $jumlah_transaksi = $model_transaksi->sisa_pembayaran;
        $connection = \Yii::$app->db;     
        $command = $connection->createCommand("UPDATE transaksi SET sisa_pembayaran=0,status='lunas',jumlah_transaksi = $jumlah_transaksi, id_staff = $id_staff, tanggal_pelunasan = '".date('Y-m-d')."' WHERE id_transaksi=$id_transaksi");
        $command->execute();   
        return $this->redirect(Url::to(['transaksi/index']));
         }else{
            throw new ForbiddenHttpException;
        }   
    }

    /**
     * Deletes an existing Pembayaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionKonfirmasi($id_transaksi,$id_pembayaran)
    {   
        if(Yii::$app->user->can('konfirmasi'))
        {
        $model_pembayaran = $this->findModel($id_pembayaran);
        $model_transaksi = Transaksi::findOne($id_transaksi);
        $sisa_pembayaran = $model_transaksi->total_pembayaran - $model_pembayaran['total_transaksi'];
        if($sisa_pembayaran <= 0)
        {
            $status = "lunas";
            $sisa_pembayaran = 0;
        }
        else
        {
            $status = "sudah dibayar";
        }

        $user_id = Yii::$app->user->id;
        $pekerja = Pekerja::findOne(['account_id' => $user_id]);  
        $id_staff = $pekerja['id_staff'];

        $connection = \Yii::$app->db;
        $command = $connection->createCommand("UPDATE transaksi SET sisa_pembayaran=$sisa_pembayaran,status='$status',status_struk='belum dikirim' WHERE id_transaksi=$id_transaksi");
        $command->execute();
        $command = $connection->createCommand("UPDATE pembayaran SET id_staff=$id_staff, tanggal_konfirmasi = '".date('Y-m-d')."' WHERE id_pembayaran=$id_pembayaran");
        $command->execute();   
        //$this->Sendmail();
        $query = (new \yii\db\Query())
        ->select(['*'])  
        ->from('transaksi')
        ->innerJoin('detail_transaksi', 'transaksi.id_transaksi = detail_transaksi.id_transaksi')
        ->innerJoin('kamar', 'detail_transaksi.id_kamar = kamar.id_kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->where(['transaksi.id_transaksi' => $id_transaksi]);

        $command = $query->createCommand();
        $models = $command->queryAll();  

        $model_pelanggan = Pelanggan::findOne(['id_pelanggan' => $models[0]['id_pelanggan']]);

        $count = (new \yii\db\Query())
        ->from('detail_transaksi')
        ->where(['detail_transaksi.id_transaksi' => $id_transaksi])
        ->count();

        return $this->render('cetak-tiket',[
                'models' => $models,
                'model_pelanggan' => $model_pelanggan,
                'jumlah_kamar' => $count,
            ]);  
         }else{
            throw new ForbiddenHttpException;
        }
    }

    public function actionTolak($id_pembayaran)
    {
        if(Yii::$app->user->can('tolak-pembayaran'))
        {
            $user_id = Yii::$app->user->id;
            $pelanggan = Pelanggan::findOne(['account_id' => $user_id]);

            $date = date('H:i');
            echo $date;
            $sapa = '';
            if ($date < 12)
                $sapa = "Pagi";
            else if ($date < 14)
                $sapa = 'Siang';
            else if ($date < 18)
                $sapa = 'Sore';

            $message = 'Pemberitahuan Penolakan Pembayaran <br>'
                    . 'Selamat ' . $sapa .' '. $pelanggan->nama.'<br>'
                    . 'Kami ingin memberitahukan bahwa pembayaran yang anda lakukan tidak diterima.'
                    . 'Silahkan cek kembali pembayaran anda dan lakukan pembayaran ulang'
                    . ' <br> <br>Atas perhatian Bapak/Ibu Sekalian, Kami Ucapkan Terimakasih'
                    . '<br><br><br> <hr> Dikirim oleh : <br>'
                    . '<b>Sistem Informasi Hotel Sere Nauli<b>';

            $tujuan = $pelanggan->email;    
            
            $email = Yii::$app->mailer->compose()
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'SIHSN'])
                    ->setTo($tujuan)
                    ->setSubject("Notifikasi Pembatalan Pembayaran")
                    ->setHtmlBody($message)
                    ->send();

            $connection = \Yii::$app->db;
            $command = $connection->createCommand("DELETE FROM pembayaran WHERE id_pembayaran  = $id_pembayaran");
            $command->execute();                
            return $this->redirect(Url::to(['transaksi/index']));
        }else{
            throw new ForbiddenHttpException;
        }        
    }

    public function actionCetakstruk($id)
    {  
        $query = (new \yii\db\Query())
        ->select(['*'])  
        ->from('transaksi')
        ->innerJoin('detail_transaksi', 'transaksi.id_transaksi = detail_transaksi.id_transaksi')
        ->innerJoin('kamar', 'detail_transaksi.id_kamar = kamar.id_kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->where(['transaksi.id_transaksi' => $id]);

        $command = $query->createCommand();
        $models = $command->queryAll();  

        $model_pelanggan = Pelanggan::findOne(['id_pelanggan' => $models[0]['id_pelanggan']]);

        $count = (new \yii\db\Query())
        ->from('detail_transaksi')
        ->where(['detail_transaksi.id_transaksi' => $id])
        ->count();

        // get your HTML raw content without any layouts or scripts
         
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $this->renderPartial('cetak-tiket',['models'=>$models,'model_pelanggan'=>$model_pelanggan,'jumlah_kamar'=>$count]),
            'filename' => 'SIHSN'.$models[0]['id_transaksi'].'.pdf',
            'cssInline' => '.invoice-box{
                            max-width:800px;
                            margin:auto;
                            padding:30px;
                            border:1px solid #eee;
                            box-shadow:0 0 10px rgba(0, 0, 0, .15);
                            font-size:16px;
                            line-height:24px;
                            font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
                            color:#555;
                            }
                            
                            .invoice-box table{
                                width:100%;
                                line-height:inherit;
                                text-align:left;
                            }
                            
                            .invoice-box table td{
                                padding:5px;
                                vertical-align:top;
                            }
                            
                            .invoice-box table tr td:nth-child(2){
                                text-align:right;
                            }
                            
                            .invoice-box table tr.top table td{
                                padding-bottom:20px;
                            }
                            
                            .invoice-box table tr.top table td.title{
                                font-size:45px;
                                line-height:45px;
                                color:#333;
                            }
                            
                            .invoice-box table tr.information table td{
                                padding-bottom:40px;
                            }
                            
                            .invoice-box table tr.heading td{
                                background:#eee;
                                border-bottom:1px solid #ddd;
                                font-weight:bold;
                            }
                            
                            .invoice-box table tr.details td{
                                padding-bottom:20px;
                            }
                            
                            .invoice-box table tr.item td{
                                border-bottom:1px solid #eee;
                            }
                            
                            .invoice-box table tr.item.last td{
                                border-bottom:none;
                            }
                            
                            .invoice-box table tr.total td:nth-child(2){
                                border-top:2px solid #eee;
                                font-weight:bold;
                            }
                            
                            @media only screen and (max-width: 600px) {
                                .invoice-box table tr.top table td{
                                    width:100%;
                                    display:block;
                                    text-align:center;
                                }
                                
                                .invoice-box table tr.information table td{
                                    width:100%;
                                    display:block;
                                    text-align:center;
                                }
                            }', 
                            // format content from your own css file if needed or use the
                            // enhanced bootstrap css built by Krajee for mPDF formatting
                            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>['Hotel Sere Na Uli'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
 
        // http response
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
 
        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Finds the Pembayaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pembayaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pembayaran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
