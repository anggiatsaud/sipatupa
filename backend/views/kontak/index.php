<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KontakSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feed back';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kontak-index">

    <br>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Tambah Feedback', ['value'=>Url::to('index.php?r=kontak%2Fcreate'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
    <?php
        Modal::begin([
                'header' =>'<h4>Tambah Feedback</h4>',
                'id'=>'modal',
                'size'=>'modal-lg'
            ]);
        echo"<div id='modalContent'></div>";

        Modal::end();
        ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_kontak',
            'email:email',
            'nama',
            'isi_kontak',
            //'status',

            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->status == 'pending')
                    {
                        return '<div>'. Html::a('Publikasikan', [
                        'kontak/publish','id'=>$model->id_kontak],
                        ['class' => 'btn btn-success']).'&nbsp;'.
                        Html::a('Delete', ['delete', 'id' => $model->id_kontak], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],]).'</div>';    
                    }
                    else if($model->status == 'publish')
                    {
                        return "<div><h4><span class='label label-info'>terpublikasi</span></h4></div>";
                    }            
                },
            ],
        ],
    ]); ?>
</div>
