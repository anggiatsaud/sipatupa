<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LaporanTransaksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Transaksis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-transaksi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Laporan Transaksi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_laporan',
            'id_transaksi',
            'tanggal_transaksi',
            'total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
