<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\LaporanTransaksi */

$this->title = 'Update Laporan Transaksi: ' . $model->id_laporan;
$this->params['breadcrumbs'][] = ['label' => 'Laporan Transaksis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_laporan, 'url' => ['view', 'id' => $model->id_laporan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="laporan-transaksi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
