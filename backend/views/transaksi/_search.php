<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use frontend\models\Transaksi;

/* @var $this yii\web\View */
/* @var $model backend\models\TransaksiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaksi-search">
    <?php Pjax::begin();?>
    <?php $form = ActiveForm::begin([
        'action'=>Url::to(['transaksi/search']),
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kode_transaksi')
        ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Transaksi::find()
        ->where(['status' => 0])
        ->all(), 'kode_transaksi', 'kode_transaksi'),
            'language' => 'en',
            'options' => ['placeholder' => 'Kode Booking'],
            'pluginOptions' => [
                'allowClear' => true
                ],
            ])->label('Kode Booking');
    ?>

    <?php // echo $form->field($model, 'id_staff') ?>

    <?php // echo $form->field($model, 'id_pelanggan') ?>

    <?php // echo $form->field($model, 'tanggal_check_in') ?>

    <?php // echo $form->field($model, 'tanggal_check_out') ?>

    <?php // echo $form->field($model, 'durasi') ?>

    <?php // echo $form->field($model, 'jumlah_kamar') ?>

    <?php // echo $form->field($model, 'jumlah_pengunjung') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'total_pembayaran') ?>

    <?php // echo $form->field($model, 'sisa_pembayaran') ?>

    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end();?>

</div>
