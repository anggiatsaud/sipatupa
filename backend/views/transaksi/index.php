<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransaksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transaksi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <h3> No rekening Pembayaran : <font color= #DE8220 >113140057293 </font>(BNI)</h3>
    <p>
<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            //'id_transaksi',
            //'id_staff',
            //'id_pelanggan',
            'tanggal_check_in',
            'tanggal_check_out',
            // 'durasi',
             'jumlah_kamar',
            // 'jumlah_pengunjung',
            // 'status',
             'total_pembayaran',
            // 'sisa_pembayaran',
            [
                'attribute' => 'Aksi',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->total_pembayaran == $model->sisa_pembayaran)
                    {
                        return '<div>'. Html::a('Bayar', [
                        'pembayaran/create','id'=>$model->id_transaksi],
                        ['class' => 'btn btn-info']).'</div>';
                    }
                    else
                    {
                        return "<div><h4><span class='label label-info'>telah dikonfirmasi</span></h4></div>";
                    }
                },
            ],

            
        ],
    ]); ?>
</div>
