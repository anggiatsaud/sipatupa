<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use frontend\models\Transaksi;

/* @var $this yii\web\View */
/* @var $model backend\models\Transaksi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaksi-form">

    <?php $form = ActiveForm::begin([
        'action'=>Url::to(['transaksi/search']),
    ]); ?>

    <?= $form->field($model, 'kode_transaksi')
        ->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Transaksi::find()
        ->where(['status' => 0])
        ->all(), 'kode_transaksi', 'kode_transaksi'),
            'language' => 'en',
            'options' => ['placeholder' => 'Kode Booking'],
            'pluginOptions' => [
                'allowClear' => true
                ],
            ])->label('Kode Booking');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Cari', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
