<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Transaksi */
/* @var $form yii\widgets\ActiveForm */


$this->title = 'Laporan';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="transaksi-form">    
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#detilKeuangan" data-toggle="tab">Detil Keuangan</a></li>
                    <li><a href="#Kamar" data-toggle="tab">Laporan Kamar</a></li>
                </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="detilKeuangan">
                    <div class="post">
                        <?php $form = ActiveForm::begin(['action'=>Url::to(['transaksi/chart'])]); ?>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label>Tanggal awal</label>
                                <?= DatePicker::widget([
                                    'model' => $model, 
                                    'attribute' => 'tanggal_check_in',
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'options' => ['placeholder' => 'Tanggal Check-in ...'],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format'=>'yyyy-mm-dd'
                                        ]
                                ]);?>
                            </div>
                            <div class="col-md-6">
                                <label>Tanggal akhir</label>
                                <?= DatePicker::widget([
                                    'model' => $model, 
                                    'attribute' => 'tanggal_check_out',
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'options' => ['placeholder' => 'Tanggal Check-in ...'],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format'=>'yyyy-mm-dd'
                                        ]
                                ]);?>           
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <div class="form-group">             
                                <?= Html::submitButton('Check',['class' => 'btn btn-warning btn-block', 'name' => 'booking-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                            <div style="display: none">
                            <?php
                            echo Highcharts::widget([
                                'scripts' => [
                                    'modules/exporting',
                                    'themes/grid-light',
                                    'highcharts-more',
                                    // 'themes/grid',
                                    'highcharts-3d',
                                    'modules/drilldown'
                                ]
                            ]);
                            ?>
                        </div>
                        <div id="chart1"></div>

                        <?php
                        $main_data = [];
                        foreach ($rawData as $booking) {
                            $main_data[] = [
                                'name' => $booking['tipe_kamar'],
                                'y' => $booking['banyak_bookingan'] * 1,
                                'drilldown' => $booking['tipe_kamar']
                            ];
                        }

                        $main = json_encode($main_data);

                        $sub_data = [];
                        foreach ($rawData as $data) {
                            $sub_data[] = [
                                'id' => $data['id_transaksi'],
                                'name' => $data['banyak_bookingan'] * 1,
                                'data' => [['tipe_kamar', $data], ['id_transaksi', $data]],
                            ];
                        }

                        $sub = json_encode($main_data);
                        ?>

                        <?php
                        $this->registerJs("$(function () {
                        
                            $('#chart1').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Data Statistik Bookingan'
                            },
                            subtitle: {
                                text: 'Hotel Sere Nauli'
                            },
                                xAxis: {
                                categories: [
                                
                                ],
                                crosshair: true
                            },
                                yAxis: {
                                 min: 0,
                                title: {
                                    text: 'Jumlah Bookingan'
                                }
                                },
                                
                                plotOptions: {

                                    series: {
                                        cursor: 'pointer',  
                                        marker: {
                                            lineWidth: 1
                                        }
                                    }
                                },

                                series: [
                                {
                                
                                   name: 'Jumlah Bookingan',
                                    colorByPoint: true,
                                    data:$main
                                },
                                
                                ],
                                drilldown: {
                                    series: $sub
                                    
                                }
                            });
                        });");
                        ?>

                        <div id="chart2"></div>

                        <?php
                        $main_data = [];
                        foreach ($rawDataTunai as $booking) {
                            $main_data[] = [
                                'name' => $booking['bulan'],
                                'y' => $booking['tunai'] * 1,
                                'drilldown' => $booking['bulan']
                            ];
                        }

                        $main = json_encode($main_data);

                        $sub_data = [];
                        foreach ($rawDataTunai as $data) {
                            $sub_data[] = [
                                'id' => $data['id_transaksi'],
                                'name' => $data['tunai'] * 1,
                                'data' => [['bulan', $data], ['id_transaksi', $data]],
                            ];
                        }

                        $sub = json_encode($main_data);
                        ?>

                        <?php
                        $this->registerJs("$(function () {
                        
                            $('#chart2').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Data Pendapatan Tunai Hotel Sere Nauli'
                            },
                            subtitle: {
                                text: 'Hotel Sere Nauli'
                            },
                                xAxis: {
                                categories: [
                                
                                ],
                                crosshair: true
                            },
                                yAxis: {
                                 min: 0,
                                title: {
                                    text: 'Pendapatan Hotel'
                                }
                                },
                                
                                plotOptions: {

                                    series: {
                                        cursor: 'pointer',  
                                        marker: {
                                            lineWidth: 1
                                        }
                                    }
                                },

                                series: [
                                {
                                
                                   name: 'Pendapatan Hotel',
                                    colorByPoint: true,
                                    data:$main
                                },
                                
                                ],
                                drilldown: {
                                    series: $sub
                                    
                                }
                            });
                        });");
                        ?>    

                        <div id="chart3"></div>

                        <?php
                        $main_data = [];
                        foreach ($rawDataTransfer as $booking) {
                            $main_data[] = [
                                'name' => $booking['bulan'],
                                'y' => $booking['transfer'] * 1,
                                'drilldown' => $booking['bulan']
                            ];
                        }

                        $main = json_encode($main_data);

                        $sub_data = [];
                        foreach ($rawDataTransfer as $data) {
                            $sub_data[] = [
                                'id' => $data['id_pembayaran'],
                                'name' => $data['transfer'] * 1,
                                'data' => [['bulan', $data], ['id_pembayaran', $data]],
                            ];
                        }

                        $sub = json_encode($main_data);
                        ?>

                        <?php
                        $this->registerJs("$(function () {
                        
                            $('#chart3').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Data Pendapatan Transfer Hotel Sere Nauli'
                            },
                            subtitle: {
                                text: 'Hotel Sere Nauli'
                            },
                                xAxis: {
                                categories: [
                                
                                ],
                                crosshair: true
                            },
                                yAxis: {
                                 min: 0,
                                title: {
                                    text: 'Pendapatan Hotel'
                                }
                                },
                                
                                plotOptions: {

                                    series: {
                                        cursor: 'pointer',  
                                        marker: {
                                            lineWidth: 1
                                        }
                                    }
                                },

                                series: [
                                {
                                
                                   name: 'Pendapatan Hotel',
                                    colorByPoint: true,
                                    data:$main
                                },
                                
                                ],
                                drilldown: {
                                    series: $sub
                                    
                                }
                            });
                        });");
                        ?> 
                    </div> 
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="Kamar">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'tipe_kamar',
                            'harga',
                            'muatan',
                            'jumlahKamar'
                        ],
                    ]); ?>
                </div><!-- /.tab-pane -->
            </div><!-- /.tab-content -->
            </div><!-- /.nav-tabs-custom -->              
        </div>              
    </div>      
</div>
