<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KamarfronSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kamarfrons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kamarfron-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kamarfron', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_kamar',
            'no_kamar',
            'id_jenis_kamar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
