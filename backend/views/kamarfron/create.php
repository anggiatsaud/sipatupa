<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Kamarfron */

$this->title = 'Create Kamarfron';
$this->params['breadcrumbs'][] = ['label' => 'Kamarfrons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kamarfron-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
