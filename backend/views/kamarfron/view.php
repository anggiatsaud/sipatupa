<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Kamarfron */

$this->title = $model->id_kamar;
$this->params['breadcrumbs'][] = ['label' => 'Kamarfrons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kamarfron-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_kamar], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_kamar], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kamar',
            'no_kamar',
            'id_jenis_kamar',
        ],
    ]) ?>

</div>
