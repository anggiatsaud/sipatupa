<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pekerja */

$this->title = 'Update Pekerja: ' . $model->id_staff;
$this->params['breadcrumbs'][] = ['label' => 'Pekerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_staff, 'url' => ['view', 'id' => $model->id_staff]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pekerja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
