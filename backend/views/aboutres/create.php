<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Aboutres */

$this->title = 'Create Aboutres';
$this->params['breadcrumbs'][] = ['label' => 'Aboutres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aboutres-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
