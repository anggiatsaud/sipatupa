<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Fasilitas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fasilitas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_fasilitas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'gambar')->fileInput() ?>

     <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
