 <?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Hotel;
use backend\models\HotelSearch;
use backend\controller\HotelController;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use backend\models\Fasilitas;
use backend\models\FasilitasSearch;
use backend\controller\FasilitasController;


use yii\helpers\Url;


$this->title = '';
?>

<div class="site-index">

 
     <div class="col-md-5" align ="right">
        <div class="box box-success">
             <div class="box-header with-border" align="right">
                <!--  -->
               <?php  
                    if ($model->gambar!='')
                        echo '<img src="'.Yii::getAlias('@web'). '/uploads/'.$model->gambar.'" width="470" height="300">'; 
                            else echo '';
                  ?>
          </div>
           </div>
       </div>
           <br>
          <div class="col-sm-4">
            
        <h1><?=
                    $model->nama_hotel; 
                ?></h1>
        <h3><?=
                    $model->alamat; 
                ?></h3>
        <h4><?=
                    $model->telepon; 
                ?></h4>
        <h4><?=
                    $model->email; 
                ?></h4>
        </div>
         <div class="col-sm-12" >
                <div class="box box-danger">
                    <div class="box-header with-border">
                      <div class="box-body with-border">
                        <h3>Deskripsi</h3>
                        <?php 
                           
                            echo $model->deskripsi;
                            
                        ?>
                       
                      </div>
                    </div>
              </div>
          </div>
    </br>
  
    <!--siswa -->
    <div class="container">
        <div class="row">

        <div class="col-lg-6">
            <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12 text-center">
                    <h2></h2>
                    <hr class="star-primary">
                </div>

            </div>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Kamar</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProviderJe,
                        'filterModel' => $searchModelJe,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Tipe Kamar',
                            'format'=>'raw',
                            
                             'value' => function ($data) {
                                    if($data->id){
                                        return $data->tipe_kamar;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['jenis-kamar/indexjeniskamar','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12 text-center">
                    <h2></h2>
                    <hr class="star-primary">
                </div>

            </div>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Fasilitas</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProviderFas,
                        'filterModel' => $searchModelFas,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Nama Fasilitas',
                            'format'=>'raw',
                            
                             'value' => function ($model) {
                                    if($model->id){
                                        return $model->nama_fasilitas;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['fasilitas/indexfasilitas','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
   
</div>
 

