<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Detailpemesanan */

$this->title = 'Create Detailpemesanan';
$this->params['breadcrumbs'][] = ['label' => 'Detailpemesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detailpemesanan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
