<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DetailpemesananSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detailpemesanans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detailpemesanan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Detailpemesanan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_pesan',
            'id_jenis_kamar',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
