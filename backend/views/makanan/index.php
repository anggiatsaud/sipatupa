<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\models\Makanan;  
use backend\controllers\MakananController;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MakananSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Makanan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="makanan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Makanan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'nama',
            // 'harga',
            'kategori',
            // 'gambar',
            //'rating',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
