<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Makanan */

$this->title = 'Create Makanan';
$this->params['breadcrumbs'][] = ['label' => 'Makanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="makanan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataMakanan' => $dataMakanan,
    ]) ?>

</div>
