<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Makanan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Makanan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="makanan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama',
            'harga',
            'kategori',
            'rating',
             [
                 'attribute' => 'Foto',
                 'format' => 'raw',
                 'value' => function ($model) {
                    if ($model->gambar!='')
                      return '<img src="'.Yii::getAlias('@web'). '/uploads/'.$model->gambar.'" width="100px" height="auto">'; else return 'no image';
                 },
            ],
        ],
    ]) ?>
</div>
