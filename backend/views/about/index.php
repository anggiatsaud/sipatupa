<?php

use yii\widgets\DetailView;
use backend\models\Hotel;
use backend\models\HotelSearch;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Carousel;
use backend\models\DefaultBlog;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AboutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SIPATUPA';
?>

<?php
$script = <<< JS
$(document).ready(function(){
    setTimeout(function() {
        $('#w0-warning').fadeOut('fast');
    }, 3000); // <-- time in milliseconds
});
JS;
$this->registerJs($script, \yii\web\View::POS_END);

?>

<div class="site-index">

    <div class="container">
        <div class="row">

            <div class="col-lg-8">

                <?php
                $items = [
                    [
                        'title' => 'Sintel',
                        'href' => 'images/slider02.jpg',
                        'type' => 'image/jpg',
                        'poster' => 'images/slider02.jpg'
                    ],

                    [
                        'url' => 'images/slider02.jpg',
                        'src' => 'images/slider02.jpg',
                        'options' => array('title' => 'Camposanto monumentale (inside)')
                    ],
                    [
                        'url' => 'images/slider03.jpg',
                        'src' => 'images/slider03.jpg',
                        'options' => array('title' => 'Hafsten - Sunset')
                    ],
                    [
                        'url' => 'images/slider04.jpg',
                        'src' => 'images/slider04.jpg',
                        'options' => array('title' => 'Hafsten - Sunset')
                    ],
                ]
                ?>

            </div>
        </div>
    </div>
    
<!--Daftar Hotel-->
<div class="container">
        <div class="row">

        <div class="col-lg-6">
            <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12 text-center">
                    <h2></h2>
                    <hr class="star-primary">
                </div>

            </div>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Hotel</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Nama Hotel',
                            'format'=>'raw',
                            
                             'value' => function ($data) {
                                    if($data->id){
                                        return $data->nama_hotel;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['hotel/indexhotel','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12 text-center">
                    <h2></h2>
                    <hr class="star-primary">
                </div>

            </div>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Restoran</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProviderRes,
                        'filterModel' => $searchModelRes,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Nama Restoran',
                            'format'=>'raw',
                            
                             'value' => function ($model) {
                                    if($model->id){
                                        return $model->nama;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['restoran/indexrestoran','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>


