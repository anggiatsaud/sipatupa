$('#w0').fullCalendar({"selectable":true,"select":function(date) {

		$.get(_opts.newUrl,{'date':date.format()},function(data){
			$('#modal').modal('show')
				.find('#modalContent')
				.html(data);			
		});			
},
"eventClick":function(calEvent) {
	$.get(_opts.updateUrl,{'id':calEvent.id},function(data){
		$('#modalUpdate').modal('show')
			.find('#modalUpdateContent')
			.html(data);			
	});	


},
"eventMouseover":function(calEvent , jsEvent , view){
	var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
	$("body").append(tooltip);
	 $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.event-tooltip').fadeIn('500');
                $('.event-tooltip').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                    $('.event-tooltip').css('top', e.pageY + 10);
                    $('.event-tooltip').css('left', e.pageX + 20);
                });
},
eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 30);
            $('.event-tooltip').remove();
        },

"events":_opts.dataEvent,"header":{"center":"title","left":"prev,next today","right":"month,agendaWeek"}});
