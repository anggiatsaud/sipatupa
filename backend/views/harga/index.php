<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\YiiAsset;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Harga Promosi';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="harga-index">

<?php

        $opts = Json::htmlEncode([
            'newUrl' => Url::to(['create']),
            'updateUrl' => Url::to(['update']),
      'dataEvent' => $events
        ]);
    $this->registerJs("var _opts = {$opts};");
    $this->registerJs($this->render('_script.js')); 
?>

  
<style>
            body {
                margin: 0px 0px;
                padding: 0;
                font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
                font-size: 14px;
            }
            .fc th {
                padding: 10px 0px;
                vertical-align: middle;
                background:#F2F2F2;
            }
            .fc-day-grid-event>.fc-content {
                padding: 4px;
            }
            #calendar {
                max-width: 500px;
                margin: 0 auto;
            }
            .error {
                color: #ac2925;
                margin-bottom: 15px;
            }
            .event-tooltip {
                width:150px;
                background: rgba(0, 0, 0, 0.85);
                color:#FFF;
                padding:10px;
                position:absolute;
                z-index:10001;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                cursor: pointer;
                font-size: 11px;
            }
        </style>

   <?php
        Modal::begin([
                'header' => '<h4>Atur Harga</h4>',
                'id' => 'modal',
                'size'=>'modal-lg',
            ]);

        echo "<div id='modalContent'></div>";
        Modal::end();   
   ?>
   <?php
      Modal::begin([
          'header' => '<h4>Update/Delete Harga</h4>',
          'id' => 'modalUpdate',
          'size' => 'modal_lg',
      ]);

      echo "<div id='modalUpdateContent'></div>";
      Modal::end();
   ?>

  <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
      'events' => $events));
  ?>
</div>
