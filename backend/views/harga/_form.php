<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Harga */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="harga-form">

    <?php $form = ActiveForm::begin(); ?>
    <label>Dari</label>
    <?= DatePicker::widget([
        'model' => $model, 
        'attribute' => 'dari',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Tanggal awal berlaku'],
        'pluginOptions' => [
        'autoclose'=>true,
        'format'=>'yyyy-mm-dd'
        ]
    ]);?>

    <label>Sampai</label>
    <?= DatePicker::widget([
        'model' => $model, 
        'attribute' => 'sampai',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Tanggal akhir berlaku'],
        'pluginOptions' => [
        'autoclose'=>true,
        'format'=>'yyyy-mm-dd'
        ]
    ]);?>

    <?= $form->field($model, 'harga')->textInput() ?>

    <?= $form->field($model, 'id_jenis_kamar')->dropDownList(
        ArrayHelper::map($jenis_kamar, 'id', 'tipe_kamar'),
        ['prompt'=>'Pilih']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
