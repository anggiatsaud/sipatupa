<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Harga */

$this->title = 'Create Harga';
$this->params['breadcrumbs'][] = ['label' => 'Hargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="harga-create">


    <?= $this->render('_form', [
        'model' => $model,
        'jenis_kamar' => $jenis_kamar,
    ]) ?>

</div>
