<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransaksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-index">
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="/ibad/demo3/demo2/plugin1/plugin/Test_1/advanced/frontend/web/assets/4515d828/images/logo.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                Invoice #: <?=$models[0]['kode_transaksi'];?><br>
                                Created: <?php echo date('Y-m-d');?><br>
                                Due: <?=$models[0]['tanggal_check_out']?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Sere Nauli Hotel<br>
                                Laguboti, Toba Samosir<br> 
                                Sumatera Utara
                            </td>
                            
                            <td>
                                Pembooking<br>
                                <?=$model_pelanggan->nama;?><br>
                                <?=$model_pelanggan->email;?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Pembayaran
                </td>
                
                <td>
                    Nominal(Rp.)
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Total Pembayaran
                </td>
                
                <td>
                    <?=$models[0]['total_pembayaran'];?>
                </td>
            </tr>

            <tr class="details">
                <td>
                    Uang Muka
                </td>
                
                <td>
                    <?=$models[0]['total_pembayaran'] - $models[0]['sisa_pembayaran'] ;?>
                </td>
            </tr>   

            <tr class="details">
                <td>
                    Sisa Pembayaran
                </td>
                
                <td>
                    <?=$models[0]['sisa_pembayaran'];?>
                </td>
            </tr>                      
            
            <tr class="heading">
                <td>
                    Detail Booking
                </td>
                
                <td>
                    Jumlah
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Tanggal Check In
                </td>
                
                <td>
                    <?=$models[0]['tanggal_check_in'];?>
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Tanggal Check Out
                </td>
                
                <td>
                    <?=$models[0]['tanggal_check_out'];?>
                </td>
            </tr>

            <tr class="item">
                <td>
                    Jumlah Pengunjung
                </td>
                
                <td>
                    <?=$models[0]['jumlah_pengunjung'] * $jumlah_kamar;?>
                </td>
            </tr>

            <tr class="item">
                <td>
                    Tipe Kamar
                </td>
                
                <td>
                    <?=$models[0]['tipe_kamar'];?>
                </td>
            </tr>            

            <?php foreach ($models as $model) {?>
            <tr class="item">
                <td>
                    No.Kamar
                </td>
                
                <td>
                    <?=$model['no_kamar'];?>
                </td>
            </tr>
            <?php } ?>

            <tr class="heading">
                <td colspan="2">
                    <center>Terimakasih Telah Membooking</center>
                </td>
            </tr>

        </table>
    </div>
    <?php
        echo Html::a('<i class="fa fa-file"></i> cetakstruk', ['/pembayaran/cetakstruk', 'id' => $models[0]['id_transaksi']], [
        'class'=>'btn btn-danger', 
        'target'=>'_blank', 
        'data-toggle'=>'tooltip', 
        'title'=>'Akan menghasilkan pdf pada halaman baru'
        ]);
    ?>
</div>

