<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model backend\models\Pembayaran */

$this->title = $model->id_pembayaran;
$this->params['breadcrumbs'][] = ['label' => 'Pembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pembayaran-view">

    

    <div class="col-md-12">
        <div class="col-md-3">
            <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
              }?> 
            <img src="<?=$backend.$model->bukti_pembayaran?>" height="225"  width="225">
        </div>
        <div class="col-md-9">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id_pembayaran',
                    //'id_transaksi',
                    //'id_pelanggan',
                    //'id_staff',
                    'no_rekening',
                    'total_transaksi',
                    //'bukti_pembayaran',
                ],
            ]) ?> 
            
             <p>
                <?= Html::a('Konfirmasi', ['konfirmasi', 'id_transaksi' => $model->id_transaksi, 'id_pembayaran'=>$model->id_pembayaran], [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => 'Apakah anda yakin untuk menerima pembayaran?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p> 
             <p>
                <?= Html::a('Tolak', ['tolak', 'id_transaksi' => $model->id_transaksi, 'id_pembayaran'=>$model->id_pembayaran], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Apakah anda yakin untuk membatalkan pembayaran?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>             
        </div>
    </div> 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        ],
    ]) ?> 
</div>
