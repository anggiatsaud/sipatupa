<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PesanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pesans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pesan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pesan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tanggal_check_in',
            'tanggal_check_out',
            'jumlah_kamar',
            'harga',

            ['class' => 'yii\grid\ActionColumn', 'template'=>'{bayar}{batal}',
                            'buttons'=>[
                              'bayar' => function ($url, $model) {     
                                return Html::a('<button>Bayar</button>', ["/Bayar/Create"], [
                                        'title' => "Bayar",
                                ]);                                
            
                              },
                               'batal' => function ($url, $model) {     
                                return Html::a('<button>Batal</button>', ["/pesan/delete"], [
                                        'title' => "Batal",
                                ]);                                
            
                              }
                          ],                            
                            ],


        ],
    ]); ?>
</div>
