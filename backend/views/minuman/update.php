<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Minuman */

$this->title = 'Update Minuman: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Minumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="minuman-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataMinuman' => $dataMinuman,
    ]) ?>

</div>
