<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Minuman;  
use backend\controllers\MinumanController;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\MinumanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Minuman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="minuman-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Minuman', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'nama',
            // 'harga',
            'kategori',
            // 'gambar',
            // //'rating',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
