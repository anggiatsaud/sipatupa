<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Minuman */

$this->title = 'Tambah Minuman';
$this->params['breadcrumbs'][] = ['label' => 'Minuman', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="minuman-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataMinuman' => $dataMinuman,
    ]) ?>

</div>
