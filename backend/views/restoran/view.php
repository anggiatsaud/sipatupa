<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Restoran */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Restorans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restoran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama',
            'alamat',
            'telepon',
            'email:email',
            'deskripsi',
             [
                'attribute' => 'Foto',
                 'format' => 'raw',
                 'value' => function ($model) {
                    if ($model->gambar!='')
                      return '<img src="'.Yii::getAlias('@web'). '/uploads/'.$model->gambar.'" width="100px" height="auto">'; else return 'no image';
                 },
            ],
        ],
    ]) ?>

</div>
