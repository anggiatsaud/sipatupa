 <?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Restoran;
use backend\models\RestoranSearch;
use backend\controller\RestoranController;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;

$this->title = '';
?>

<div class="site-index">

 
     <div class="col-md-5" align ="right">
        <div class="box box-success">
             <div class="box-header with-border" align="right">
                <!--  -->
               <?php  
                    if ($model->gambar!='')
                        echo '<img src="'.Yii::getAlias('@web'). '/uploads/'.$model->gambar.'" width="470" height="300">'; 
                            else echo '';
                  ?>
          </div>
           </div>
       </div>
           <br>
          <div class="col-sm-4">
            
        <h1><?=
                    $model->nama; 
                ?></h1>
        <h3><?=
                    $model->alamat; 
                ?></h3>
        <h3><?=
                    $model->telepon; 
                ?></h3>
        <h3><?=
                    $model->email; 
                ?></h3>
                        
        </div>
         <div class="col-sm-12" >
                <div class="box box-danger">
                    <div class="box-header with-border">
                      <div class="box-body with-border">
                        <h3>Deskripsi</h3>
                        <?php 
                           
                            echo $model->deskripsi;
                            
                        ?>
                       
                      </div>
                    </div>
              </div>
          </div>
    </br>
  
    <!--siswa -->
    <div class="container">
            <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12 text-center">
                    <h2></h2>
                    <hr class="star-primary">
                </div>

            </div>
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Makanan</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProviderMa,
                        'filterModel' => $searchModelMa,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Nama Makanan',
                            'format'=>'raw',
                            
                             'value' => function ($data) {
                                    if($data->id){
                                        return $data->nama;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['makanan/indexmakanan','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
     
           
            <div class="col-md-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Minuman</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProviderMi,
                        'filterModel' => $searchModelMi,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Nama Minuman',
                            'format'=>'raw',
                            
                             'value' => function ($model) {
                                    if($model->id){
                                        return $model->nama;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['minuman/indexminuman','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
      
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Fasilitas</h4>
                    </div>
                    <div class="panel-body">
                         <?php
                      echo GridView::widget([
                        'dataProvider' => $dataProviderFas,
                        'filterModel' => $searchModelFas,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            [
                             'attribute' => 'Nama Fasilitas',
                            'format'=>'raw',
                            
                             'value' => function ($model) {
                                    if($model->id){
                                        return $model->nama_fasilitas;
                                    }
                                    }
                                
                             ],
                             
                            
                             
                             [

                                    'class' => 'yii\grid\ActionColumn',
                                    'template' =>'{view}',
                                    'buttons' =>[
                                        'view' => function ($url, $model){
                                            
                            
                                                return Html::a('<span class="btn btn-success btn-xs"><i class="fa fa-eye"></i></span>', $url, [
                                                    'title' => Yii::t('app', 'Lihat'),
                                                ]);
                                            
                                        },
                                    ],
                                    'urlCreator' => function ($action, $model, $key, $index){
                                        if($action === 'view'){

                                            return Url::toRoute(['fasilitas/indexfasilitas','id' =>$model->id]);
                                        }
                                    }
                             ],
                         ],
                    ]);
                 ?>
                    </div>
                </div>
            </div>
       
 
    </div>

</div>
 

