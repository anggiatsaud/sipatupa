<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\models\Restoran;    
use backend\controllers\RestoranController;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SekolahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Restoran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restoran-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'nama',
            'alamat',
            'telepon',
            'email',
            
            // [
            //      'attribute' => 'Foto Sekolah',
            //      'format' => 'raw',
            //      'value' => function ($model) {
            //         if ($model->foto_sekolah!='')
            //           return '<img src="'.Yii::getAlias('@web'). '/gambarsekolah/'.$model->foto_sekolah.'" width="100px" height="auto">'; else return 'no image';
            //      },
            // ],

            // 'misi',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',
            // 'deleted',
            // 'created_by',
            // 'updated_by',
            // 'deleted_by',
             
        ],
    ]); 
?>
</div>
