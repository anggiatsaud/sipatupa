<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\JenisKamar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-kamar-form">

    <?php $form = ActiveForm::begin(); ?>   

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'tipe_kamar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gambar')->fileInput() ?>
    
    <?= $form->field($model, 'harga')->textInput() ?>

    <?= $form->field($model, 'muatan')->textInput() ?>

    <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>

     <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
