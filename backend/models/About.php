<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $about_judul
 * @property string $about_deskripsi
 * @property string $gambar
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['about_judul', 'about_deskripsi'], 'required'],
            [['about_judul', 'about_deskripsi'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'about_judul' => 'About Judul',
            'about_deskripsi' => 'About Deskripsi',
        ];
    }
}
