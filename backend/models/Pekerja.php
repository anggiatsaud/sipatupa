<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pekerja".
 *
 * @property int $id_staff
 * @property int $id_user
 * @property string $nama
 * @property string $jabatan
 * @property int $no_telphone
 * @property string $alamat
 * @property string $email
 *
 * @property Kontak $kontak
 * @property Pembayaran $pembayaran
 * @property Transaksi $transaksi
 */
class Pekerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pekerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'nama', 'jabatan', 'no_telphone', 'alamat', 'email'], 'required'],
            [['id_user', 'no_telphone'], 'integer'],
            [['nama'], 'string', 'max' => 100],
            [['jabatan'], 'string', 'max' => 50],
            [['alamat'], 'string', 'max' => 250],
            [['email'], 'string', 'max' => 60],
            [['id_user'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_staff' => 'Id Staff',
            'id_user' => 'Id User',
            'nama' => 'Nama',
            'jabatan' => 'Jabatan',
            'no_telphone' => 'No Telphone',
            'alamat' => 'Alamat',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKontak()
    {
        return $this->hasOne(Kontak::className(), ['id_staff' => 'id_staff']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayaran()
    {
        return $this->hasOne(Pembayaran::className(), ['id_staff' => 'id_staff']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksi()
    {
        return $this->hasOne(Transaksi::className(), ['id_karyawan' => 'id_staff']);
    }
}
