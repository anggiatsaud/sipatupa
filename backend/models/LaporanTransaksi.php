<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "laporan_transaksi".
 *
 * @property int $id_laporan
 * @property int $id_transaksi
 * @property string $tanggal_transaksi
 * @property string $total
 */
class LaporanTransaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'laporan_transaksi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transaksi', 'tanggal_transaksi', 'total'], 'required'],
            [['id_transaksi'], 'integer'],
            [['tanggal_transaksi'], 'safe'],
            [['total'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_laporan' => 'Id Laporan',
            'id_transaksi' => 'Id Transaksi',
            'tanggal_transaksi' => 'Tanggal Transaksi',
            'total' => 'Total',
        ];
    }
}
