<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "minuman".
 *
 * @property int $id
 * @property string $nama
 * @property int $harga
 * @property string $kategori
 * @property string $gambar
 * @property string $rating
 */
class Minuman extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'minuman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'harga', 'kategori', 'gambar', 'rating'], 'required'],
            [['harga'], 'integer'],
            [['nama', 'kategori', 'gambar', 'rating'], 'string', 'max' => 100],
            [['gambar'], 'file', 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'kategori' => 'Kategori',
            'gambar' => 'Gambar',
            'rating' => 'Rating',
        ];
    }
}
