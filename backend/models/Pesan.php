<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pesan".
 *
 * @property int $id
 * @property string $tanggal_check_in
 * @property string $tanggal_check_out
 * @property string $jumlah_kamar
 * @property double $total_pembayaran
 */
class Pesan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pesan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tanggal_check_in', 'tanggal_check_out', 'jumlah_kamar', 'harga'], 'required'],
            [['tanggal_check_in', 'tanggal_check_out'], 'safe'],
            [['harga'], 'number'],
            [['jumlah_kamar'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tanggal_check_in' => 'Tanggal Check In',
            'tanggal_check_out' => 'Tanggal Check Out',
            'jumlah_kamar' => 'Jumlah Kamar',
            'harga' => 'Harga',
        ];
    }
}
