<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "hotels".
 *
 * @property int $id
 * @property string $nama_hotel
 * @property string $alamat
 * @property string $telepon
 * @property string $email
 * @property string $deskripsi
 * @property string $gambar
 */
class Hotel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hotels';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_hotel', 'alamat', 'telepon', 'email', 'deskripsi', 'gambar'], 'required'],
            [['nama_hotel', 'alamat', 'deskripsi', 'gambar'], 'string', 'max' => 255],
            [['telepon'], 'string', 'max' => 64],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_hotel' => 'Nama Hotel',
            'alamat' => 'Alamat',
            'telepon' => 'Telepon',
            'email' => 'Email',
            'deskripsi' => 'Deskripsi',
            'gambar' => 'Gambar',
        ];
    }
}
