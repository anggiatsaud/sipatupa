<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pembayaran".
 *
 * @property integer $id_pembayaran
 * @property integer $id_transaksi
 * @property integer $id_pelanggan
 * @property integer $id_staff
 * @property string $no_rekening
 * @property double $total_transaksi
 * @property string $bukti_pembayaran
 *
 * @property Transaksi $idTransaksi
 */
class Pembayaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pembayaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transaksi', 'id_pelanggan', 'id_staff'], 'integer'],
            [['total_transaksi'], 'number'],
            [['no_rekening', 'bukti_pembayaran'], 'string', 'max' => 60],
            [['id_transaksi'], 'exist', 'skipOnError' => true, 'targetClass' => Transaksi::className(), 'targetAttribute' => ['id_transaksi' => 'id_transaksi']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pembayaran' => 'Id Pembayaran',
            'id_transaksi' => 'Id Transaksi',
            'id_pelanggan' => 'Id Pelanggan',
            'id_staff' => 'Id Staff',
            'no_rekening' => 'No Rekening',
            'total_transaksi' => 'Total Transaksi',
            'bukti_pembayaran' => 'Bukti Pembayaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTransaksi()
    {
        return $this->hasOne(Transaksi::className(), ['id_transaksi' => 'id_transaksi']);
    }
}
