<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "harga".
 *
 * @property integer $id
 * @property string $dari
 * @property string $sampai
 * @property double $harga
 * @property integer $id_jenis_kamar
 */
class Harga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'harga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dari', 'sampai'], 'safe'],
            [['harga'], 'number'],
            [['id_jenis_kamar'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dari' => 'Dari',
            'sampai' => 'Sampai',
            'harga' => 'Harga',
            'id_jenis_kamar' => 'Id Jenis Kamar',
        ];
    }
}
