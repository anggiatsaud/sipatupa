<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pembayaran;

/**
 * PembayaranSearch represents the model behind the search form about `backend\models\Pembayaran`.
 */
class PembayaranSearch extends Pembayaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pembayaran', 'id_transaksi', 'id_pelanggan', 'id_staff'], 'integer'],
            [['no_rekening', 'bukti_pembayaran'], 'safe'],
            [['total_transaksi'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pembayaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pembayaran' => $this->id_pembayaran,
            'id_transaksi' => $this->id_transaksi,
            'id_pelanggan' => $this->id_pelanggan,
            'id_staff' => $this->id_staff,
            'total_transaksi' => $this->total_transaksi,
        ]);

        $query->andFilterWhere(['like', 'no_rekening', $this->no_rekening])
            ->andFilterWhere(['like', 'bukti_pembayaran', $this->bukti_pembayaran]);

        return $dataProvider;
    }
}
