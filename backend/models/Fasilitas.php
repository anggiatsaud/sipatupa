<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "fasilitas".
 *
 * @property int $id
 * @property string $nama_fasilitas
 * @property string $deskripsi
 * @property double $harga
 * @property string $gambar
 */
class Fasilitas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fasilitas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_fasilitas', 'deskripsi', 'harga'], 'required'],
            [['nama_fasilitas', 'gambar'], 'string', 'max' => 64],
            [['deskripsi'], 'string', 'max' => 2500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_fasilitas' => 'Nama Fasilitas',
            'deskripsi' => 'Deskripsi',
            'gambar' => 'Gambar',
        ];
    }
}
