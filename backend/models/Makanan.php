<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "makanan".
 *
 * @property int $id
 * @property string $nama
 * @property int $harga
 * @property int $kategori
 * @property string $gambar
 * @property string $rating
 */
class Makanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'makanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'harga', 'kategori', 'gambar', 'rating'], 'required'],
            [['harga', 'kategori'], 'integer'],
            [['nama', 'gambar'], 'string', 'max' => 255],
            [['rating'], 'string', 'max' => 100],
            // [['id'], 'unique'],
            [['gambar'], 'file', 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'kategori' => 'Kategori',
            'gambar' => 'Gambar',
            'rating' => 'Rating',
        ];
    }
}
