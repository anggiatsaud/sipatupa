<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pelanggan".
 *
 * @property integer $id_pelanggan
 * @property integer $account_id
 * @property string $nama
 * @property string $email
 * @property string $no_telephone
 * @property string $alamat
 */
class Pelanggan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pelanggan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'required'],
            [['account_id'], 'integer'],
            [['nama', 'email', 'no_telephone', 'alamat'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pelanggan' => 'Id Pelanggan',
            'account_id' => 'Account ID',
            'nama' => 'Nama',
            'email' => 'Email',
            'no_telephone' => 'No Telephone',
            'alamat' => 'Alamat',
        ];
    }
}
