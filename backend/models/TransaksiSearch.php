<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Transaksi;

/**
 * TransaksiSearch represents the model behind the search form about `backend\models\Transaksi`.
 */
class TransaksiSearch extends Transaksi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transaksi','durasi', 'jumlah_kamar', 'jumlah_pengunjung'], 'integer'],
            [['tanggal_check_in', 'tanggal_check_out', 'status'], 'safe'],
            [['total_pembayaran', 'sisa_pembayaran'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transaksi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transaksi' => $this->id_transaksi,
            'tanggal_check_in' => $this->tanggal_check_in,
            'tanggal_check_out' => $this->tanggal_check_out,
            'durasi' => $this->durasi,
            'jumlah_kamar' => $this->jumlah_kamar,
            'jumlah_pengunjung' => $this->jumlah_pengunjung,
            'total_pembayaran' => $this->total_pembayaran,
            'sisa_pembayaran' => $this->sisa_pembayaran,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
