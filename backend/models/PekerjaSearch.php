<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Pekerja;

/**
 * PekerjaSearch represents the model behind the search form of `backend\models\Pekerja`.
 */
class PekerjaSearch extends Pekerja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_staff', 'id_user', 'no_telphone'], 'integer'],
            [['nama', 'jabatan', 'alamat', 'email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pekerja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_staff' => $this->id_staff,
            'id_user' => $this->id_user,
            'no_telphone' => $this->no_telphone,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jabatan', $this->jabatan])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
