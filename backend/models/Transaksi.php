<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property integer $id_transaksi
 * @property integer $id_staff
 * @property integer $id_pelanggan
 * @property string $kode_transaksi
 * @property string $tanggal_check_in
 * @property string $tanggal_check_out
 * @property integer $durasi
 * @property integer $jumlah_kamar
 * @property integer $jumlah_pengunjung
 * @property string $status
 * @property double $total_pembayaran
 * @property double $sisa_pembayaran
 *
 * @property Pembayaran[] $pembayarans
 */
class Transaksi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaksi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['durasi', 'jumlah_kamar', 'jumlah_pengunjung'], 'integer'],
            [['id_pelanggan', 'kode_transaksi', 'durasi', 'jumlah_kamar', 'jumlah_pengunjung'], 'required'],
            [['tanggal_check_in', 'tanggal_check_out'], 'safe'],
            [['total_pembayaran', 'sisa_pembayaran'], 'number'],
            [['kode_transaksi'], 'string', 'max' => 64],
            [['status'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_transaksi' => 'Id Transaksi',
            'kode_transaksi' => 'Kode Transaksi',
            'tanggal_check_in' => 'Tanggal Check In',
            'tanggal_check_out' => 'Tanggal Check Out',
            'durasi' => 'Durasi',
            'jumlah_kamar' => 'Jumlah Kamar',
            'jumlah_pengunjung' => 'Jumlah Pengunjung',
            'status' => 'Status',
            'total_pembayaran' => 'Total Pembayaran',
            'sisa_pembayaran' => 'Sisa Pembayaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPembayarans()
    {
        return $this->hasMany(Pembayaran::className(), ['id_transaksi' => 'id_transaksi']);
    }
}
