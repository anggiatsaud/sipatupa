<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "detailpemesanan".
 *
 * @property int $id
 * @property int $id_pesan
 * @property int $id_jenis_kamar
 *
 * @property JenisKamar $jenisKamar
 * @property Pesan $pesan
 */
class Detailpemesanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detailpemesanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pesan', 'id_jenis_kamar'], 'required'],
            [['id_pesan', 'id_jenis_kamar'], 'integer'],
            [['id_pesan'], 'unique'],
            [['id_jenis_kamar'], 'unique'],
            [['id_jenis_kamar'], 'exist', 'skipOnError' => true, 'targetClass' => JenisKamar::className(), 'targetAttribute' => ['id_jenis_kamar' => 'id']],
            [['id_pesan'], 'exist', 'skipOnError' => true, 'targetClass' => Pesan::className(), 'targetAttribute' => ['id_pesan' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pesan' => 'Id Pesan',
            'id_jenis_kamar' => 'Id Jenis Kamar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisKamar()
    {
        return $this->hasOne(JenisKamar::className(), ['id' => 'id_jenis_kamar']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesan()
    {
        return $this->hasOne(Pesan::className(), ['id' => 'id_pesan']);
    }
}
