<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jenis_kamar".
 *
 * @property int $id
 * @property string $tipe_kamar
 * @property string $gambar
 * @property double $harga
 * @property int $muatan
 * @property string $deskripsi
 *
 * @property Harga $harga0
 * @property Kamar $kamar
 */
class JenisKamar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_kamar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','harga', 'muatan', 'deskripsi'], 'required'],
            [['id','muatan'], 'integer'],
            [['harga'], 'number'],
            [['tipe_kamar'], 'string', 'max' => 64],
            [['gambar'], 'string', 'max' => 255],
            [['deskripsi'], 'string', 'max' => 200],
            // [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipe_kamar' => 'Tipe Kamar',
            'gambar' => 'Gambar',
            'harga' => 'Harga',
            'muatan' => 'Muatan',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHarga()
    {
        return $this->hasOne(Harga::className(), ['id_jenis_kamar' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKamar()
    {
        return $this->hasOne(Kamar::className(), ['id_jenis_kamar' => 'id']);
    }
    
     public function getKamars()
    {
        return $this->hasOne(Kamar::className(), ['id_jenis_kamar' => 'id']);
    }
}
