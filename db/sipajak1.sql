-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 12:35 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipajak1`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `about_judul` varchar(250) NOT NULL,
  `about_deskripsi` varchar(250) NOT NULL,
  `gambar` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aboutres`
--

CREATE TABLE `aboutres` (
  `id` int(11) NOT NULL,
  `about_judul` varchar(250) NOT NULL,
  `about_deskripsi` varchar(250) NOT NULL,
  `gambar` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1527343257),
('adminrestoran', '3', 1527331820),
('gemar', '10', 1527591096),
('kasirgemar', '13', 1527640705),
('kepaladinas', '14', 1527655416),
('petugasdinas', '12', 1527598638),
('Receptionist', '2', 1527321404),
('resepsionisserenauli', '11', 1527590484),
('serenauli', '4', 1527574390),
('user', '15', 1527654436);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/about/*', 2, NULL, NULL, NULL, 1527505155, 1527505155),
('/about/create', 2, NULL, NULL, NULL, 1527488976, 1527488976),
('/about/delete', 2, NULL, NULL, NULL, 1527488976, 1527488976),
('/about/index', 2, NULL, NULL, NULL, 1527483093, 1527483093),
('/about/update', 2, NULL, NULL, NULL, 1527488976, 1527488976),
('/about/view', 2, NULL, NULL, NULL, 1527488975, 1527488975),
('/aboutres/*', 2, NULL, NULL, NULL, 1527517672, 1527517672),
('/aboutres/create', 2, NULL, NULL, NULL, 1527517527, 1527517527),
('/aboutres/delete', 2, NULL, NULL, NULL, 1527517616, 1527517616),
('/aboutres/index', 2, NULL, NULL, NULL, 1527517413, 1527517413),
('/aboutres/update', 2, NULL, NULL, NULL, 1527517564, 1527517564),
('/aboutres/view', 2, NULL, NULL, NULL, 1527517485, 1527517485),
('/admin/assignment/assign', 2, NULL, NULL, NULL, 1527908906, 1527908906),
('/admin/assignment/index', 2, NULL, NULL, NULL, 1527908791, 1527908791),
('/admin/assignment/revoke', 2, NULL, NULL, NULL, 1527908914, 1527908914),
('/admin/assignment/view', 2, NULL, NULL, NULL, 1527908896, 1527908896),
('/admin/menu/create', 2, NULL, NULL, NULL, 1527911381, 1527911381),
('/admin/menu/delete', 2, NULL, NULL, NULL, 1527911382, 1527911382),
('/admin/menu/index', 2, NULL, NULL, NULL, 1527911381, 1527911381),
('/admin/menu/update', 2, NULL, NULL, NULL, 1527911381, 1527911381),
('/admin/menu/view', 2, NULL, NULL, NULL, 1527911381, 1527911381),
('/admin/permission/assign', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/permission/create', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/permission/delete', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/permission/index', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/permission/remove', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/permission/update', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/permission/view', 2, NULL, NULL, NULL, 1527909528, 1527909528),
('/admin/role/assign', 2, NULL, NULL, NULL, 1527909879, 1527909879),
('/admin/role/create', 2, NULL, NULL, NULL, 1527909879, 1527909879),
('/admin/role/delete', 2, NULL, NULL, NULL, 1527909879, 1527909879),
('/admin/role/index', 2, NULL, NULL, NULL, 1527909878, 1527909878),
('/admin/role/remove', 2, NULL, NULL, NULL, 1527909879, 1527909879),
('/admin/role/update', 2, NULL, NULL, NULL, 1527909879, 1527909879),
('/admin/role/view', 2, NULL, NULL, NULL, 1527909879, 1527909879),
('/admin/route/assign', 2, NULL, NULL, NULL, 1527909295, 1527909295),
('/admin/route/create', 2, NULL, NULL, NULL, 1527909295, 1527909295),
('/admin/route/index', 2, NULL, NULL, NULL, 1527909295, 1527909295),
('/admin/route/refresh', 2, NULL, NULL, NULL, 1527909295, 1527909295),
('/admin/route/remove', 2, NULL, NULL, NULL, 1527909295, 1527909295),
('/admin/user/activate', 2, NULL, NULL, NULL, 1527911192, 1527911192),
('/admin/user/change-password', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/delete', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/index', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/login', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/logout', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/reset-password', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/signup', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/admin/user/view', 2, NULL, NULL, NULL, 1527911191, 1527911191),
('/fasilitas/create', 2, NULL, NULL, NULL, 1527580929, 1527580929),
('/fasilitas/daftarfasilitas', 2, NULL, NULL, NULL, 1528050562, 1528050562),
('/fasilitas/delete', 2, NULL, NULL, NULL, 1527578798, 1527578798),
('/fasilitas/index', 2, NULL, NULL, NULL, 1527578746, 1527578746),
('/fasilitas/indexfasilitas', 2, NULL, NULL, NULL, 1528050584, 1528050584),
('/fasilitas/update', 2, NULL, NULL, NULL, 1527578789, 1527578789),
('/fasilitas/view', 2, NULL, NULL, NULL, 1527578761, 1527578761),
('/hotel/create', 2, NULL, NULL, NULL, 1527319038, 1527319038),
('/hotel/daftarhotel', 2, NULL, NULL, NULL, 1527501919, 1527501919),
('/hotel/delete', 2, NULL, NULL, NULL, 1527501433, 1527501433),
('/hotel/index', 2, NULL, NULL, NULL, 1527342997, 1527342997),
('/hotel/indexhotel', 2, NULL, NULL, NULL, 1527501843, 1527501843),
('/hotel/update', 2, NULL, NULL, NULL, 1527501439, 1527501439),
('/hotel/view', 2, NULL, NULL, NULL, 1527501429, 1527501429),
('/item/create', 2, NULL, NULL, NULL, 1527565987, 1527565987),
('/item/index', 2, NULL, NULL, NULL, 1527567068, 1527567068),
('/jenis-kamar/create', 2, NULL, NULL, NULL, 1527576750, 1527576750),
('/jenis-kamar/daftarjenis-kamar', 2, NULL, NULL, NULL, 1528092277, 1528092277),
('/jenis-kamar/delete', 2, NULL, NULL, NULL, 1527574941, 1527574941),
('/jenis-kamar/index', 2, NULL, NULL, NULL, 1527574871, 1527574871),
('/jenis-kamar/indexjenis-kamar', 2, NULL, NULL, NULL, 1528092652, 1528092652),
('/jenis-kamar/indexjeniskamar', 2, NULL, NULL, NULL, 1528095474, 1528095474),
('/jenis-kamar/update', 2, NULL, NULL, NULL, 1527574920, 1527574920),
('/jenis-kamar/view', 2, NULL, NULL, NULL, 1527574889, 1527574889),
('/kamar/create', 2, NULL, NULL, NULL, 1527574231, 1527574231),
('/kamar/delete', 2, NULL, NULL, NULL, 1527574276, 1527574276),
('/kamar/index', 2, NULL, NULL, NULL, 1527574306, 1527574306),
('/kamar/update', 2, NULL, NULL, NULL, 1527574288, 1527574288),
('/kamar/view', 2, NULL, NULL, NULL, 1527574266, 1527574266),
('/karyawan/create', 2, NULL, NULL, NULL, 1527579234, 1527579234),
('/karyawan/delete', 2, NULL, NULL, NULL, 1527579280, 1527579280),
('/karyawan/index', 2, NULL, NULL, NULL, 1527579172, 1527579172),
('/karyawan/update', 2, NULL, NULL, NULL, 1527579255, 1527579255),
('/karyawan/view', 2, NULL, NULL, NULL, 1527579219, 1527579219),
('/laporan-transaksi/create', 2, NULL, NULL, NULL, 1527598302, 1527598302),
('/laporan-transaksi/delete', 2, NULL, NULL, NULL, 1527598356, 1527598356),
('/laporan-transaksi/index', 2, NULL, NULL, NULL, 1527598288, 1527598288),
('/laporan-transaksi/update', 2, NULL, NULL, NULL, 1527598400, 1527598400),
('/laporan-transaksi/view', 2, NULL, NULL, NULL, 1527598319, 1527598319),
('/makanan/create', 2, NULL, NULL, NULL, 1527591754, 1527591754),
('/makanan/delete', 2, NULL, NULL, NULL, 1527591777, 1527591777),
('/makanan/index', 2, NULL, NULL, NULL, 1527591722, 1527591722),
('/makanan/indexmakanan', 2, NULL, NULL, NULL, 1528098839, 1528098839),
('/makanan/update', 2, NULL, NULL, NULL, 1527591764, 1527591764),
('/makanan/view', 2, NULL, NULL, NULL, 1527591739, 1527591739),
('/minuman/create', 2, NULL, NULL, NULL, 1527592181, 1527592181),
('/minuman/delete', 2, NULL, NULL, NULL, 1527592215, 1527592215),
('/minuman/index', 2, NULL, NULL, NULL, 1527592192, 1527592192),
('/minuman/indexminuman', 2, NULL, NULL, NULL, 1528098851, 1528098851),
('/minuman/view', 2, NULL, NULL, NULL, 1527593377, 1527593377),
('/pekerja/create', 2, NULL, NULL, NULL, 1527614966, 1527614966),
('/pekerja/delete', 2, NULL, NULL, NULL, 1527615002, 1527615002),
('/pekerja/index', 2, NULL, NULL, NULL, 1527614945, 1527614945),
('/pekerja/update', 2, NULL, NULL, NULL, 1527614980, 1527614980),
('/pekerja/view', 2, NULL, NULL, NULL, 1527614957, 1527614957),
('/r/j', 2, NULL, NULL, NULL, 1527909455, 1527909455),
('/restoran/create', 2, NULL, NULL, NULL, 1527321037, 1527321037),
('/restoran/daftarrestoran', 2, NULL, NULL, NULL, 1527502002, 1527502002),
('/restoran/delete', 2, NULL, NULL, NULL, 1527573939, 1527573939),
('/restoran/index', 2, NULL, NULL, NULL, 1527321024, 1527321024),
('/restoran/indexrestoran', 2, NULL, NULL, NULL, 1527501700, 1527501700),
('/restoran/update', 2, NULL, NULL, NULL, 1527516217, 1527516217),
('/restoran/view', 2, NULL, NULL, NULL, 1527516128, 1527516128),
('/transaction/create', 2, NULL, NULL, NULL, 1527567489, 1527567489),
('/transaction/delete', 2, NULL, NULL, NULL, 1527590862, 1527590862),
('/transaction/index', 2, NULL, NULL, NULL, 1527567467, 1527567467),
('/transaction/update', 2, NULL, NULL, NULL, 1527590840, 1527590840),
('/transaction/view', 2, NULL, NULL, NULL, 1527590933, 1527590933),
('/transaksi/create', 2, NULL, NULL, NULL, 1527587496, 1527587496),
('/transaksi/delete', 2, NULL, NULL, NULL, 1527587512, 1527587512),
('/transaksi/index', 2, NULL, NULL, NULL, 1527587447, 1527587447),
('/transaksi/update', 2, NULL, NULL, NULL, 1527587503, 1527587503),
('/transaksi/view', 2, NULL, NULL, NULL, 1527587486, 1527587486),
('about', 2, NULL, NULL, NULL, 1527505169, 1527505169),
('aboutcreate', 2, NULL, NULL, NULL, 1527489128, 1527489128),
('aboutdelete', 2, NULL, NULL, NULL, 1527489359, 1527489359),
('aboutindex', 2, NULL, NULL, NULL, 1527483181, 1527483181),
('aboutres', 2, NULL, NULL, NULL, 1527517684, 1527517684),
('aboutrescreate', 2, NULL, NULL, NULL, 1527517538, 1527517538),
('aboutresdelete', 2, NULL, NULL, NULL, 1527517638, 1527517638),
('aboutresindex', 2, NULL, NULL, NULL, 1527517435, 1527517435),
('aboutresupdate', 2, NULL, NULL, NULL, 1527517580, 1527517580),
('aboutresview', 2, NULL, NULL, NULL, 1527517496, 1527517496),
('aboutupdate', 2, NULL, NULL, NULL, 1527489263, 1527489263),
('aboutview', 2, NULL, NULL, NULL, 1527489024, 1527489024),
('admin', 1, NULL, NULL, NULL, 1527341830, 1527341830),
('adminrestoran', 1, NULL, NULL, NULL, 1527331783, 1527331783),
('assigmentindex', 2, NULL, NULL, NULL, 1527909006, 1527909006),
('createdelete', 2, NULL, NULL, NULL, 1527574597, 1527574597),
('createview', 2, NULL, NULL, NULL, 1527574472, 1527574472),
('daftarfasilitas', 2, NULL, NULL, NULL, 1528050604, 1528050604),
('daftarjenis_kamar', 2, NULL, NULL, NULL, 1528092292, 1528092292),
('fasilitascreate', 2, NULL, NULL, NULL, 1527580958, 1527580958),
('fasilitasdelete', 2, NULL, NULL, NULL, 1527579019, 1527579019),
('fasilitasindex', 2, NULL, NULL, NULL, 1527578817, 1527578817),
('fasilitasupdate', 2, NULL, NULL, NULL, 1527578980, 1527578980),
('fasilitasview', 2, NULL, NULL, NULL, 1527578874, 1527578874),
('gemar', 1, NULL, NULL, NULL, 1527591065, 1527591065),
('hotel/indexhotel', 2, NULL, NULL, NULL, 1527504608, 1527504608),
('hotelcreate', 2, NULL, NULL, NULL, 1527319074, 1527319074),
('hoteldaftarhotel', 2, NULL, NULL, NULL, 1527501942, 1527501942),
('hoteldelete', 2, NULL, NULL, NULL, 1527343548, 1527343548),
('hotelindex', 2, NULL, NULL, NULL, 1527319722, 1527319722),
('hotelindexhotel', 2, NULL, NULL, NULL, 1527501885, 1527501885),
('hotelupdate', 2, NULL, NULL, NULL, 1527343434, 1527343434),
('hotelview', 2, NULL, NULL, NULL, 1527343179, 1527343179),
('indexfasilitas', 2, NULL, NULL, NULL, 1528055586, 1528055586),
('indexjeniskamar', 2, NULL, NULL, NULL, 1528092671, 1528092671),
('indexmakanan', 2, NULL, NULL, NULL, 1528098895, 1528098895),
('indexminuman', 2, NULL, NULL, NULL, 1528103118, 1528103118),
('itemcreate', 2, NULL, NULL, NULL, 1527566025, 1527566025),
('itemview', 2, NULL, NULL, NULL, 1527566793, 1527567107),
('jenis-kamarcreate', 2, NULL, NULL, NULL, 1527575045, 1527575045),
('jenis-kamardelete', 2, NULL, NULL, NULL, 1527575227, 1527575227),
('jenis-kamarindex', 2, NULL, NULL, NULL, 1527574980, 1527574980),
('jenis-kamarupdate', 2, NULL, NULL, NULL, 1527575175, 1527575175),
('jenis-kamarview', 2, NULL, NULL, NULL, 1527575078, 1527575078),
('kamarcreate', 2, NULL, NULL, NULL, 1527574412, 1527670387),
('kamardelete', 2, NULL, NULL, NULL, 1527574701, 1527574701),
('kamarindex', 2, NULL, NULL, NULL, 1527574329, 1527574329),
('kamarupdate', 2, NULL, NULL, NULL, 1527574748, 1527574748),
('kamarview', 2, NULL, NULL, NULL, 1527574539, 1527574539),
('karyawancreate', 2, NULL, NULL, NULL, 1527579988, 1527579988),
('karyawandelete', 2, NULL, NULL, NULL, 1527580273, 1527580273),
('karyawanindex', 2, NULL, NULL, NULL, 1527580083, 1527580083),
('karyawanupdate', 2, NULL, NULL, NULL, 1527580230, 1527580230),
('karyawanview', 2, NULL, NULL, NULL, 1527580159, 1527580159),
('kasir', 1, NULL, NULL, NULL, 1527313808, 1527313808),
('kasirgemar', 1, NULL, NULL, NULL, 1527640658, 1527640658),
('kepaladinas', 1, NULL, NULL, NULL, 1527644238, 1527655386),
('laporan-transaksicreate', 2, NULL, NULL, NULL, 1527598789, 1527598789),
('laporan-transaksidelete', 2, NULL, NULL, NULL, 1527598893, 1527598893),
('laporan-transaksiindex', 2, NULL, NULL, NULL, 1527598475, 1527598475),
('laporan-transaksiview', 2, NULL, NULL, NULL, 1527598848, 1527598848),
('makanancreate', 2, NULL, NULL, NULL, 1527332020, 1527332020),
('makanandelete', 2, NULL, NULL, NULL, 1527332246, 1527332246),
('makananindex', 2, NULL, NULL, NULL, 1527331399, 1527331399),
('makananview', 2, NULL, NULL, NULL, 1527593528, 1527593528),
('menuindex', 2, NULL, NULL, NULL, 1527911397, 1527911397),
('minumancreate', 2, NULL, NULL, NULL, 1527592387, 1527592387),
('minumandelete', 2, NULL, NULL, NULL, 1527592469, 1527592469),
('minumanindex', 2, NULL, NULL, NULL, 1527592338, 1527592338),
('minumanview', 2, NULL, NULL, NULL, 1527593333, 1527593333),
('pekerjacreate', 2, NULL, NULL, NULL, 1527615076, 1527615076),
('pekerjadelete', 2, NULL, NULL, NULL, 1527615287, 1527615287),
('pekerjaindex', 2, NULL, NULL, NULL, 1527615020, 1527615020),
('pekerjaupdate', 2, NULL, NULL, NULL, 1527615248, 1527615248),
('pekerjaview', 2, NULL, NULL, NULL, 1527615177, 1527615177),
('permissionsindex', 2, NULL, NULL, NULL, 1527909546, 1527909546),
('petugasdinas', 1, NULL, NULL, NULL, 1527598584, 1527598584),
('Receptionist', 1, NULL, NULL, NULL, 1527321231, 1527321231),
('requestpassword', 2, NULL, NULL, NULL, 1527912101, 1527912101),
('resepsionisserenauli', 1, NULL, NULL, NULL, 1527590444, 1527590444),
('resetpassword', 2, NULL, NULL, NULL, 1527911991, 1527911991),
('restoran/create', 2, NULL, NULL, NULL, 1527321129, 1527321129),
('restoran/daftarrestoran', 2, NULL, NULL, NULL, 1527502019, 1527502019),
('restoran/delete', 2, NULL, NULL, NULL, 1527389086, 1527389086),
('restoran/index', 2, NULL, NULL, NULL, 1527321087, 1527321087),
('restoran/indexrestoran', 2, NULL, NULL, NULL, 1527501724, 1527501724),
('restoran/update', 2, NULL, NULL, NULL, 1527321164, 1527321164),
('restoran/view', 2, NULL, NULL, NULL, 1527388880, 1527388880),
('restorandelete', 2, NULL, NULL, NULL, 1527516272, 1527516272),
('restoranupdate', 2, NULL, NULL, NULL, 1527516235, 1527516235),
('restoranview', 2, NULL, NULL, NULL, 1527516151, 1527516151),
('roleindex', 2, NULL, NULL, NULL, 1527910678, 1527910678),
('routeindex', 2, NULL, NULL, NULL, 1527909314, 1527909314),
('serenauli', 1, NULL, NULL, NULL, 1527574361, 1527574361),
('transactioncreate', 2, NULL, NULL, NULL, 1527567527, 1527567527),
('transactiondelete', 2, NULL, NULL, NULL, 1527591119, 1527591119),
('transactionindex', 2, NULL, NULL, NULL, 1527567556, 1527567556),
('transactionupdate', 2, NULL, NULL, NULL, 1527591211, 1527591211),
('transactionview', 2, NULL, NULL, NULL, 1527591026, 1527591026),
('transaksicreate', 2, NULL, NULL, NULL, 1527587667, 1527587667),
('transaksidelete', 2, NULL, NULL, NULL, 1527587871, 1527587871),
('transaksiindex', 2, NULL, NULL, NULL, 1527587579, 1527587579),
('transaksiupdate', 2, NULL, NULL, NULL, 1527587797, 1527587797),
('transaksiview', 2, NULL, NULL, NULL, 1527587729, 1527587729),
('ubahpassword', 2, NULL, NULL, NULL, 1527911873, 1527911873),
('user', 1, NULL, NULL, NULL, 1527654405, 1527654405),
('userindex', 2, NULL, NULL, NULL, 1527911202, 1527911202),
('usersignup', 2, NULL, NULL, NULL, 1527911502, 1527911502);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('about', '/about/*'),
('aboutcreate', '/about/create'),
('aboutdelete', '/about/delete'),
('aboutindex', '/about/index'),
('aboutres', '/aboutres/*'),
('aboutrescreate', '/aboutres/create'),
('aboutresdelete', '/aboutres/delete'),
('aboutresindex', '/aboutres/index'),
('aboutresupdate', '/aboutres/update'),
('aboutresview', '/aboutres/view'),
('aboutupdate', '/about/update'),
('aboutview', '/about/view'),
('admin', 'about'),
('admin', 'aboutcreate'),
('admin', 'aboutdelete'),
('admin', 'aboutindex'),
('admin', 'aboutres'),
('admin', 'aboutrescreate'),
('admin', 'aboutresdelete'),
('admin', 'aboutresindex'),
('admin', 'aboutresupdate'),
('admin', 'aboutresview'),
('admin', 'aboutupdate'),
('admin', 'aboutview'),
('admin', 'assigmentindex'),
('admin', 'hotel/indexhotel'),
('admin', 'hotelcreate'),
('admin', 'hoteldaftarhotel'),
('admin', 'hoteldelete'),
('admin', 'hotelindex'),
('admin', 'hotelindexhotel'),
('admin', 'hotelupdate'),
('admin', 'hotelview'),
('admin', 'menuindex'),
('admin', 'permissionsindex'),
('admin', 'requestpassword'),
('admin', 'resetpassword'),
('admin', 'restoran/create'),
('admin', 'restoran/daftarrestoran'),
('admin', 'restoran/delete'),
('admin', 'restoran/index'),
('admin', 'restoran/indexrestoran'),
('admin', 'restoran/update'),
('admin', 'restoran/view'),
('admin', 'restorandelete'),
('admin', 'restoranupdate'),
('admin', 'restoranview'),
('admin', 'roleindex'),
('admin', 'routeindex'),
('admin', 'ubahpassword'),
('admin', 'userindex'),
('admin', 'usersignup'),
('adminrestoran', 'makanancreate'),
('adminrestoran', 'makanandelete'),
('adminrestoran', 'makananindex'),
('assigmentindex', '/admin/assignment/index'),
('createdelete', '/kamar/delete'),
('createview', '/kamar/view'),
('daftarfasilitas', '/fasilitas/daftarfasilitas'),
('daftarjenis_kamar', '/jenis-kamar/daftarjenis-kamar'),
('fasilitascreate', '/fasilitas/create'),
('fasilitasdelete', '/fasilitas/delete'),
('fasilitasindex', '/fasilitas/index'),
('fasilitasupdate', '/fasilitas/update'),
('fasilitasview', '/fasilitas/view'),
('gemar', 'itemcreate'),
('gemar', 'itemview'),
('gemar', 'karyawancreate'),
('gemar', 'karyawandelete'),
('gemar', 'karyawanindex'),
('gemar', 'karyawanupdate'),
('gemar', 'karyawanview'),
('gemar', 'makanancreate'),
('gemar', 'makanandelete'),
('gemar', 'makananindex'),
('gemar', 'makananview'),
('gemar', 'minumancreate'),
('gemar', 'minumandelete'),
('gemar', 'minumanindex'),
('gemar', 'minumanview'),
('hotel/indexhotel', '/hotel/indexhotel'),
('hotelcreate', '/hotel/create'),
('hoteldaftarhotel', '/hotel/daftarhotel'),
('hoteldelete', '/hotel/delete'),
('hotelindex', '/hotel/index'),
('hotelindexhotel', '/hotel/indexhotel'),
('hotelupdate', '/hotel/update'),
('hotelview', '/hotel/view'),
('indexfasilitas', '/fasilitas/indexfasilitas'),
('indexjeniskamar', '/jenis-kamar/indexjeniskamar'),
('indexmakanan', '/makanan/indexmakanan'),
('indexminuman', '/minuman/indexminuman'),
('itemcreate', '/item/create'),
('itemview', '/item/index'),
('jenis-kamarcreate', '/jenis-kamar/create'),
('jenis-kamardelete', '/jenis-kamar/delete'),
('jenis-kamarindex', '/jenis-kamar/index'),
('jenis-kamarupdate', '/jenis-kamar/update'),
('jenis-kamarview', '/jenis-kamar/view'),
('kamarcreate', '/kamar/create'),
('kamardelete', '/kamar/delete'),
('kamarindex', '/kamar/index'),
('kamarupdate', '/kamar/update'),
('kamarview', '/kamar/view'),
('karyawancreate', '/karyawan/create'),
('karyawandelete', '/karyawan/delete'),
('karyawanindex', '/karyawan/index'),
('karyawanupdate', '/karyawan/update'),
('karyawanview', '/karyawan/view'),
('kasir', 'hotelcreate'),
('kasir', 'hotelindex'),
('kasirgemar', 'transactioncreate'),
('kasirgemar', 'transactiondelete'),
('kasirgemar', 'transactionindex'),
('kasirgemar', 'transactionupdate'),
('kasirgemar', 'transactionview'),
('kepaladinas', 'laporan-transaksicreate'),
('kepaladinas', 'laporan-transaksidelete'),
('kepaladinas', 'laporan-transaksiindex'),
('kepaladinas', 'laporan-transaksiview'),
('kepaladinas', 'transactioncreate'),
('kepaladinas', 'transactiondelete'),
('kepaladinas', 'transactionindex'),
('kepaladinas', 'transactionupdate'),
('kepaladinas', 'transactionview'),
('kepaladinas', 'transaksicreate'),
('kepaladinas', 'transaksidelete'),
('kepaladinas', 'transaksiindex'),
('kepaladinas', 'transaksiupdate'),
('kepaladinas', 'transaksiview'),
('laporan-transaksidelete', 'laporan-transaksiview'),
('laporan-transaksiindex', '/transaction/create'),
('makanancreate', '/makanan/create'),
('makanandelete', '/makanan/delete'),
('makananindex', '/makanan/index'),
('makananview', '/makanan/view'),
('menuindex', '/admin/menu/index'),
('minumancreate', '/minuman/create'),
('minumandelete', '/minuman/delete'),
('minumanindex', '/minuman/index'),
('minumanview', '/minuman/view'),
('pekerjacreate', '/pekerja/create'),
('pekerjadelete', '/pekerja/delete'),
('pekerjaindex', '/pekerja/index'),
('pekerjaupdate', '/pekerja/update'),
('pekerjaview', '/pekerja/view'),
('permissionsindex', '/admin/permission/index'),
('petugasdinas', 'laporan-transaksicreate'),
('petugasdinas', 'laporan-transaksidelete'),
('petugasdinas', 'laporan-transaksiindex'),
('petugasdinas', 'laporan-transaksiview'),
('petugasdinas', 'transactioncreate'),
('petugasdinas', 'transactiondelete'),
('petugasdinas', 'transactionindex'),
('petugasdinas', 'transactionupdate'),
('petugasdinas', 'transactionview'),
('petugasdinas', 'transaksicreate'),
('petugasdinas', 'transaksidelete'),
('petugasdinas', 'transaksiindex'),
('petugasdinas', 'transaksiupdate'),
('petugasdinas', 'transaksiview'),
('Receptionist', 'restoran/create'),
('Receptionist', 'restoran/index'),
('Receptionist', 'restoran/update'),
('requestpassword', '/admin/user/request-password-reset'),
('resepsionisserenauli', 'transaksicreate'),
('resepsionisserenauli', 'transaksidelete'),
('resepsionisserenauli', 'transaksiindex'),
('resepsionisserenauli', 'transaksiupdate'),
('resepsionisserenauli', 'transaksiview'),
('resetpassword', '/admin/user/reset-password'),
('restoran/create', '/restoran/create'),
('restoran/daftarrestoran', '/restoran/daftarrestoran'),
('restoran/index', '/restoran/index'),
('restoran/indexrestoran', '/restoran/indexrestoran'),
('restoran/view', '/restoran/view'),
('restoranupdate', '/restoran/update'),
('restoranview', '/restoran/view'),
('roleindex', '/admin/role/index'),
('routeindex', '/admin/route/index'),
('serenauli', 'fasilitascreate'),
('serenauli', 'fasilitasdelete'),
('serenauli', 'fasilitasindex'),
('serenauli', 'fasilitasupdate'),
('serenauli', 'fasilitasview'),
('serenauli', 'jenis-kamarcreate'),
('serenauli', 'jenis-kamardelete'),
('serenauli', 'jenis-kamarindex'),
('serenauli', 'jenis-kamarupdate'),
('serenauli', 'jenis-kamarview'),
('serenauli', 'kamarcreate'),
('serenauli', 'kamardelete'),
('serenauli', 'kamarindex'),
('serenauli', 'kamarupdate'),
('serenauli', 'kamarview'),
('serenauli', 'karyawancreate'),
('serenauli', 'karyawandelete'),
('serenauli', 'karyawanindex'),
('serenauli', 'karyawanupdate'),
('serenauli', 'karyawanview'),
('serenauli', 'pekerjacreate'),
('serenauli', 'pekerjadelete'),
('serenauli', 'pekerjaindex'),
('serenauli', 'pekerjaupdate'),
('serenauli', 'pekerjaview'),
('serenauli', 'restorandelete'),
('transactioncreate', '/transaction/create'),
('transactiondelete', '/transaction/delete'),
('transactionindex', '/transaction/index'),
('transactionupdate', '/transaksi/update'),
('transactionview', '/transaction/view'),
('transaksicreate', '/transaksi/create'),
('transaksidelete', '/transaksi/delete'),
('transaksiindex', '/transaksi/index'),
('transaksiupdate', '/transaksi/update'),
('transaksiview', '/transaksi/view'),
('ubahpassword', '/admin/user/change-password'),
('user', 'about'),
('user', 'aboutcreate'),
('user', 'aboutdelete'),
('user', 'aboutindex'),
('user', 'aboutres'),
('user', 'aboutrescreate'),
('user', 'aboutresdelete'),
('user', 'aboutresindex'),
('user', 'aboutresupdate'),
('user', 'aboutresview'),
('user', 'aboutupdate'),
('user', 'aboutview'),
('user', 'daftarfasilitas'),
('user', 'daftarjenis_kamar'),
('user', 'hotel/indexhotel'),
('user', 'hotelindexhotel'),
('user', 'indexfasilitas'),
('user', 'indexjeniskamar'),
('user', 'indexmakanan'),
('user', 'indexminuman'),
('user', 'restoran/indexrestoran'),
('userindex', '/admin/user/index'),
('usersignup', '/admin/user/signup');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_kamar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE `fasilitas` (
  `id` int(11) NOT NULL,
  `nama_fasilitas` varchar(64) NOT NULL,
  `deskripsi` varchar(2500) NOT NULL,
  `gambar` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`id`, `nama_fasilitas`, `deskripsi`, `gambar`) VALUES
(3, 'Speed Boat', 'Membatu para pengunjung untuk melihat keindahan Danau Toba', 'rt.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id` int(11) NOT NULL,
  `dari` date DEFAULT NULL,
  `sampai` date DEFAULT NULL,
  `harga` date DEFAULT NULL,
  `id_jenis_kamar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(100) NOT NULL,
  `nama_hotel` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `nama_hotel`, `alamat`, `telepon`, `email`, `deskripsi`, `gambar`) VALUES
(1, 'Sere Nauli', 'Jl. Sisingamangaraja No. 1, Kec. Laguboti, Ps. Laguboti, Lagu Boti, Kabupaten Toba Samosir, Sumatera Utara 22381', '(0632) 331003', 'serenauli@gmail.com', 'Hotel berbintang yang berani dipinggiran desa', 'download (1).jpg'),
(2, 'Mareda', 'baligee', '', '', 'mantap', 'sumbangan.jpg'),
(6, 'ompuHerti', 'baligeh', '', '', 'hasas', '1.png'),
(8, 'TaraBunga', 'baligeee', '', '', 'adad', 'rt.jpg'),
(9, 'Sere Nauli', 'Laguboti', '', '', 'Hotel berbintang yang berani dipinggiran desa', 'del.png'),
(10, 'djasdjas', 'hadasdh', '', '', 'ssdhaskjdas', 'green-home-icon.png'),
(11, 'Mezra', 'Balige', '', '', 'Hotel berbintang yang berani dipinggiran desa', 'del.png');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` int(11) DEFAULT '0',
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `code`, `name`, `quantity`, `remarks`) VALUES
(1, 'MK1', 'Nasi Goreng', 100, 'Item Create');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kamar`
--

CREATE TABLE `jenis_kamar` (
  `id` int(11) NOT NULL,
  `tipe_kamar` varchar(64) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `harga` double NOT NULL,
  `muatan` int(11) NOT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kamar`
--

INSERT INTO `jenis_kamar` (`id`, `tipe_kamar`, `gambar`, `harga`, `muatan`, `deskripsi`) VALUES
(2, 'Luxio', 'green-home-icon.png', 2000000, 4, 'Bagus'),
(8, 'Luxio', '4.png', 2000000, 4, 'yuiop[]');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `id_kamar` int(11) NOT NULL,
  `no_kamar` int(11) DEFAULT NULL,
  `id_jenis_kamar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id_kamar`, `no_kamar`, `id_jenis_kamar`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id_kontak` int(11) NOT NULL,
  `email` varchar(60) DEFAULT NULL,
  `nama` varchar(60) DEFAULT NULL,
  `isi_kontak` varchar(200) DEFAULT NULL,
  `ratings` double DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `id_staff` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `laporan_transaksi`
--

CREATE TABLE `laporan_transaksi` (
  `id_laporan` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `total` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `makanan`
--

CREATE TABLE `makanan` (
  `id` int(100) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` int(100) NOT NULL,
  `kategori` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `rating` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `makanan`
--

INSERT INTO `makanan` (`id`, `nama`, `harga`, `kategori`, `gambar`, `rating`) VALUES
(1, 'Mie Goreng', 10000, 1, 'green-home-icon.png', '3'),
(2, 'Bakmie', 10000, 2, 'green-home-icon.png', 'Enak');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `parent`, `route`, `order`, `data`) VALUES
(3, 'Tambah Hotel', 4, '/hotel/create', NULL, NULL),
(4, 'Hotel', NULL, NULL, NULL, NULL),
(5, 'Restoran', NULL, NULL, NULL, NULL),
(6, 'Daftar Restoran', 5, '/restoran/index', NULL, NULL),
(7, 'Tambah Restoran', 5, '/restoran/create', NULL, NULL),
(8, 'Makanan', NULL, NULL, NULL, NULL),
(9, 'Daftar Makanan', 8, '/makanan/index', NULL, NULL),
(10, 'Tambah Makanan', 8, '/makanan/create', NULL, NULL),
(12, 'Daftar Hotel', 4, '/hotel/index', NULL, NULL),
(16, 'Home', NULL, '/about/index', NULL, NULL),
(18, 'Item', NULL, NULL, NULL, NULL),
(19, 'Tambah Item', 18, '/item/create', NULL, NULL),
(21, 'Kamar', NULL, NULL, NULL, NULL),
(22, 'Daftar Kamar', 21, '/kamar/index', NULL, NULL),
(23, 'Tambah Kamar', 21, '/kamar/create', NULL, NULL),
(36, 'Tambah Jenis Kamar', 21, '/jenis-kamar/create', NULL, NULL),
(37, 'Daftar Jenis Kamar', 21, '/jenis-kamar/index', NULL, NULL),
(38, 'Fasilitas', NULL, NULL, NULL, NULL),
(39, 'Daftar Fasilitas', 38, '/fasilitas/index', NULL, NULL),
(41, 'Tambah Fasilitas', 38, '/fasilitas/create', NULL, NULL),
(42, 'Karyawan', NULL, NULL, NULL, NULL),
(43, 'Daftar Karyawan', 42, '/pekerja/index', NULL, NULL),
(44, 'Tambah Karyawan', 42, '/pekerja/create', NULL, NULL),
(46, 'Transaksi Hotel', NULL, '/transaksi/index', NULL, NULL),
(51, 'Minuman', NULL, NULL, NULL, NULL),
(52, 'Daftar Minuman', 51, '/minuman/index', NULL, NULL),
(53, 'Tambah Minuman', 51, '/minuman/create', NULL, NULL),
(54, 'Laporan', NULL, NULL, NULL, NULL),
(55, 'Daftar Laporan', 54, '/laporan-transaksi/index', NULL, NULL),
(57, 'Transaksi Restoran', NULL, '/transaction/index', NULL, NULL),
(58, 'Item', NULL, NULL, NULL, NULL),
(60, 'Daftar Item', 18, '/item/index', NULL, NULL),
(61, 'Laporan', NULL, NULL, NULL, NULL),
(62, 'Manajemen User', NULL, NULL, NULL, NULL),
(63, 'Assignment', 62, '/admin/assignment/index', NULL, NULL),
(64, 'Route', 62, '/admin/route/index', NULL, NULL),
(65, 'Permissions', 62, '/admin/permission/index', NULL, NULL),
(66, 'Role', 62, '/admin/role/index', NULL, NULL),
(67, 'User', 62, '/admin/user/index', NULL, NULL),
(68, 'Menu', 62, '/admin/menu/index', NULL, NULL),
(70, 'Users', NULL, NULL, NULL, NULL),
(71, 'Daftar Role', 70, '/admin/user/signup', NULL, NULL),
(72, 'Ubah Password ', 70, '/admin/user/change-password', NULL, NULL),
(73, 'Reset Password', 70, '/admin/user/reset-password', NULL, NULL),
(74, 'Request Password', 70, '/admin/user/request-password-reset', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1527302538),
('m140506_102106_rbac_init', 1527303736),
('m140602_111327_create_menu_table', 1527302719),
('m160312_050000_create_user', 1527302719),
('m180528_030654_create_table_items', 1527564522),
('m180528_030718_create_table_transaction_types', 1527564522),
('m180528_030735_create_table_transactions', 1527564524),
('m180528_030812_create_table_transaction_details', 1527564526);

-- --------------------------------------------------------

--
-- Table structure for table `minuman`
--

CREATE TABLE `minuman` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `rating` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `minuman`
--

INSERT INTO `minuman` (`id`, `nama`, `harga`, `kategori`, `gambar`, `rating`) VALUES
(1, 'Jus', 10000, '1', 'has', '212'),
(3, 'Jus Jeruk', 10000, '1', 'del.png', '3');

-- --------------------------------------------------------

--
-- Table structure for table `pekerja`
--

CREATE TABLE `pekerja` (
  `id_staff` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `no_telphone` int(11) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pekerja`
--

INSERT INTO `pekerja` (`id_staff`, `id_user`, `nama`, `jabatan`, `no_telphone`, `alamat`, `email`) VALUES
(1, 1, 'adas', 'asdas', 8000, 'jfgfd', 'hna@gmail.cm');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama` varchar(60) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `no_telephone` varchar(60) DEFAULT NULL,
  `alamat` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `id_staff` int(11) DEFAULT NULL,
  `no_rekening` varchar(60) DEFAULT NULL,
  `total_transaksi` double DEFAULT NULL,
  `bukti_pembayaran` varchar(60) DEFAULT NULL,
  `tanggal_konfirmasi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `rating` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `restoran`
--

CREATE TABLE `restoran` (
  `id` int(100) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restoran`
--

INSERT INTO `restoran` (`id`, `nama`, `alamat`, `telepon`, `email`, `deskripsi`, `gambar`) VALUES
(1, 'Smile Coffee', 'balige', '', '', 'bagus', 'Acara Firewall.png'),
(2, 'Boruku', 'Soposurung', '', '', 'Punya pak Hutabarat', 'green-home-icon.png'),
(3, 'Bapena Cafe', 'Lintas Sumatra', '', '', 'Luar biasa', 'del.png'),
(4, 'Bakso Babi', 'Balige', '', '', 'Enak', 'del.png'),
(5, 'Boyma', 'Laguboti', '', '', 'khas batak', 'del.png'),
(6, 'Gemar', 'Balige', '082168209240', 'gemar@gmail.com', 'Ayo', 'download (2).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `trans_code` varchar(255) NOT NULL,
  `trans_date` date NOT NULL DEFAULT '0000-00-00',
  `type_id` int(11) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `trans_code`, `trans_date`, `type_id`, `remarks`) VALUES
(1, '123456', '2018-05-29', 2, ''),
(2, '123456', '2018-05-29', 2, 'b fgffhhgjhjhkjl');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE `transaction_details` (
  `id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,2) NOT NULL DEFAULT '0.00',
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_details`
--

INSERT INTO `transaction_details` (`id`, `trans_id`, `item_id`, `quantity`, `remarks`) VALUES
(1, 1, 1, '2.00', ''),
(2, 2, 1, '2.00', 'kjsnjnsn;');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_types`
--

CREATE TABLE `transaction_types` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_types`
--

INSERT INTO `transaction_types` (`id`, `code`, `name`) VALUES
(1, 'IN', 'Barang Masuk'),
(2, 'OUT', 'Barang Keluar');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `kode_transaksi` varchar(64) NOT NULL,
  `tanggal_check_in` date DEFAULT NULL,
  `tanggal_check_out` date DEFAULT NULL,
  `durasi` int(11) NOT NULL,
  `jumlah_kamar` int(11) NOT NULL,
  `jumlah_pengunjung` int(11) NOT NULL,
  `status` varchar(60) DEFAULT NULL,
  `total_pembayaran` double DEFAULT NULL,
  `sisa_pembayaran` double DEFAULT NULL,
  `jumlah_transaksi` double DEFAULT NULL,
  `tanggal_pelunasan` date DEFAULT NULL,
  `tanggal_booking` date DEFAULT NULL,
  `status_struk` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'pfQOuUDr9RVD9Xip3pTh8o7C4yQt-xGy', '$2y$13$Q21Q80oJvNp7DPgjdhodxem4XVC7ClTAjzzXVR3cZFRojR02shf5a', NULL, 'admin@gmail.com', 10, 1527304568, 1527304568),
(2, 'resepsionis', 'PZ_Ws2Wp0C7KPKhScJSvyLCLwfnmf_HV', '$2y$13$Voe/p/.b/WTI6syixMVXNO7JpPLqHCkHAjUEsBsfhN90prAhmEIs6', NULL, 'resepsionis@gmail.com', 10, 1527321360, 1527321360),
(4, 'serenauli', 'byJ9_qqGlzQY0gaJmaLStgdbD2FSrD1x', '$2y$13$EMmzA6i212ktfQucSMEdyeRloRjA88y7lXBvO6w6iINH8iM2C7HoS', NULL, 'serenauli@gmail.com', 10, 1527390467, 1527390467),
(6, 'boruku', 'tZCaF22gKxHSVyTOciVA-vcXJFqoGOeK', '$2y$13$E6oOPz62/eVCmJCScvXd/OWELDXWOzQ3m9tZn3aqOxdFSNnpCExUu', NULL, 'boruku@gmail.com', 10, 1527390973, 1527390973),
(7, 'hans', 'kH5cBJWGeuBWDpf60oebIhThfAbH4OlA', '$2y$13$dAc0xqHhM6v.dAhMWcO3uO0ICAUlTCzoP1YZHcmVYIUmWg3afO4YG', NULL, 'hans@gmail.com', 10, 1527561887, 1527561887),
(8, 'greace', 'QYKbCMEBjV0HUZg23MJsapvtkXyzjws1', '$2y$13$jcP2DDzF9bZsIWPtwa682ObGkPZ1byrbIMsWt6cdqEb73ceuGxK1G', NULL, 'greace@gmail.com', 10, 1527561903, 1527561903),
(9, 'winda', 'eiWuHPl6jqWYJovI-A4DfmfGVHeuL3Cf', '$2y$13$J0itfebz/Z.N/jF0GRHpKefwCN47F/N8PZF7RSIMHIra8YMKhzopC', NULL, 'winda@gmail.com', 10, 1527561915, 1527561915),
(10, 'gemar', 'b4gplkQx0WxHuJkADIkYsT8gDI1mwUJJ', '$2y$13$.qsuwYOePj969wu6ROwgHuii8Xv/5YqIekbBQTw/KNnjfovoEnCgW', NULL, 'gemar@gmail.com', 10, 1527574006, 1527574006),
(11, 'resepsionisserenauli', 'RF26CAXMMhOPYhC-huUD7-klwCWP_Du_', '$2y$13$rSBRX25AF/Pr6Kvz5MEyzeGkZDPaLB/mG7RcDgKMoENOOjhEI6ryC', NULL, 'resepsionisserenauli@gmail.com', 10, 1527590348, 1527590348),
(12, 'petugasdinas', 'ZzK2_8-Sl_11lWoGbunkQHzTAWOWky5b', '$2y$13$2KZKNAeWwi9wnBVqVTeST.FMzmnbUsNlJOgTSQlO9jNMRIyn7HdV2', NULL, 'petugasdinas@gmail.com', 10, 1527597554, 1527597554),
(13, 'kasirgemar', '7Xit263hQu_3DcutTqrP1oydl8LGHA08', '$2y$13$dLPnjCcrEB3oAYBs2fq.X.7trCRz7YVcuhJTLU1ga7SevbvUm0RBS', NULL, 'kasirgemar@gmail.com', 10, 1527640509, 1527640509),
(14, 'kepaladinas', 'ioP4LQwdo6ooUtAFsau4rqxBIGTj5ntO', '$2y$13$fROq5.p.nH3tfRfaTlJ2V.839wARgNr2DhLfsByd9Std1GY/JH1sm', NULL, 'kepaladinas@gmail.com', 10, 1527643963, 1527643963),
(15, 'user', 'H0okAiGz9WnJBr6Fisv9sDx5j_pjG_2m', '$2y$13$f8HSRT8gmJ8jGgpInM4Ho.f7xrFpZ.2jPbqwWKaBG4I1YQRIPTbZm', NULL, 'user@gmail.com', 10, 1527654360, 1527654360);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aboutres`
--
ALTER TABLE `aboutres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaksi` (`id_transaksi`),
  ADD UNIQUE KEY `id_kamar` (`id_kamar`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_jenis_kamar` (`id_jenis_kamar`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_kamar`
--
ALTER TABLE `jenis_kamar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`id_kamar`),
  ADD UNIQUE KEY `id_jenis_kamar` (`id_jenis_kamar`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id_kontak`),
  ADD UNIQUE KEY `id_karyawan` (`id_staff`);

--
-- Indexes for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  ADD PRIMARY KEY (`id_laporan`);

--
-- Indexes for table `makanan`
--
ALTER TABLE `makanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `minuman`
--
ALTER TABLE `minuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pekerja`
--
ALTER TABLE `pekerja`
  ADD PRIMARY KEY (`id_staff`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`),
  ADD UNIQUE KEY `account_id` (`id_user`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD UNIQUE KEY `id_transaksi` (`id_transaksi`),
  ADD UNIQUE KEY `id_pelanggan` (`id_pelanggan`),
  ADD UNIQUE KEY `id_karyawan` (`id_staff`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restoran`
--
ALTER TABLE `restoran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transactions_types` (`type_id`);

--
-- Indexes for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transaction_details_transactions` (`trans_id`),
  ADD KEY `fk_transaction_details_items` (`item_id`);

--
-- Indexes for table `transaction_types`
--
ALTER TABLE `transaction_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD UNIQUE KEY `id_pelanggan` (`id_pelanggan`),
  ADD UNIQUE KEY `id_pegawai` (`id_karyawan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fasilitas`
--
ALTER TABLE `fasilitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id_kontak` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `laporan_transaksi`
--
ALTER TABLE `laporan_transaksi`
  MODIFY `id_laporan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `makanan`
--
ALTER TABLE `makanan`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `minuman`
--
ALTER TABLE `minuman`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pekerja`
--
ALTER TABLE `pekerja`
  MODIFY `id_staff` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `restoran`
--
ALTER TABLE `restoran`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transaction_types`
--
ALTER TABLE `transaction_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_1` FOREIGN KEY (`id_kamar`) REFERENCES `kamar` (`id_kamar`),
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`);

--
-- Constraints for table `harga`
--
ALTER TABLE `harga`
  ADD CONSTRAINT `harga_ibfk_1` FOREIGN KEY (`id_jenis_kamar`) REFERENCES `jenis_kamar` (`id`);

--
-- Constraints for table `kamar`
--
ALTER TABLE `kamar`
  ADD CONSTRAINT `kamar_ibfk_1` FOREIGN KEY (`id_jenis_kamar`) REFERENCES `jenis_kamar` (`id`);

--
-- Constraints for table `kontak`
--
ALTER TABLE `kontak`
  ADD CONSTRAINT `kontak_ibfk_1` FOREIGN KEY (`id_staff`) REFERENCES `pekerja` (`id_staff`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD CONSTRAINT `pelanggan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`),
  ADD CONSTRAINT `pembayaran_ibfk_2` FOREIGN KEY (`id_staff`) REFERENCES `pekerja` (`id_staff`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `fk_transactions_types` FOREIGN KEY (`type_id`) REFERENCES `transaction_types` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD CONSTRAINT `fk_transaction_details_items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transaction_details_transactions` FOREIGN KEY (`trans_id`) REFERENCES `transactions` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_karyawan`) REFERENCES `pekerja` (`id_staff`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
