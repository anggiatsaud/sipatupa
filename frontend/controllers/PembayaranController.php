<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Pembayaran;
use frontend\models\PembayaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;
use common\models\Pelanggan;
use frontend\models\Transaksi;

/**
 * PembayaranController implements the CRUD actions for Pembayaran model.
 */
class PembayaranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pembayaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PembayaranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pembayaran model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pembayaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Pembayaran();
        $user_id = Yii::$app->user->id;
        $pelanggan = Pelanggan::findOne(['account_id' => $user_id]);
        $transaksi = Transaksi::findOne(['id_transaksi' => $id]);
        $total_pembayaran = $transaksi->total_pembayaran;
        $harus_dibayar = (25/100) * $total_pembayaran;

        if ($model->load(Yii::$app->request->post())) {
            if($model->total_transaksi < $harus_dibayar)
            {
                Yii::$app->session->setFlash('danger','Maaf anda harus membayar minimal 25% untuk uang muka');
                return $this->render('create', [
                    'model' => $model,
                    ]);
            }
            else
            {
                $model->save();  
                $imageName = $model->id_pembayaran;
                $model->file = UploadedFile::getInstance($model,'file');
                $backend = "";
                $path = Url::to("@backend/web/images/pembayarans/");
                for ($i = 0; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
                }
                $model->file->saveAs($backend.$imageName.'.'.$model->file->extension);
                $model->bukti_pembayaran = 'images/pembayarans/'.$imageName.'.'.$model->file->extension;                
                $model->id_pelanggan = $pelanggan['id_pelanggan'];
                $model->id_transaksi = $id;
                $model->save();  
                return $this->redirect(['view', 'id' => $model->id_pembayaran]);
            }           
        } else {
            return $this->render('create', [
                'model' => $model,
                'harus_dibayar' => $harus_dibayar,
                ]);
            }
        }

    /**
     * Updates an existing Pembayaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pembayaran]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pembayaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pembayaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pembayaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pembayaran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}