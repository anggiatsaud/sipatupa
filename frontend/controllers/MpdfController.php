<?php
namespace frontend\controllers;
 
use Yii;
use yii\web\Controller;
use kartik\mpdf\pdf;
use yii\helpers\Url;
use frontend\models\Pelanggan;
 
class MpdfController extends Controller
{
    public function actionCetakTiket()
    {
        $query = (new \yii\db\Query())
        ->select(['*'])  
        ->from('transaksi')
        ->innerJoin('detail_transaksi', 'transaksi.id_transaksi = detail_transaksi.id_transaksi')
        ->innerJoin('kamar', 'detail_transaksi.id_kamar = kamar.id_kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->where(['transaksi.id_transaksi' => 95]);

        $command = $query->createCommand();
        $models = $command->queryAll();  


        $count = (new \yii\db\Query())
        ->from('detail_transaksi')
        ->where(['detail_transaksi.id_transaksi' => 95])
        ->count();

        return $this->render('cetak-tiket',[
                'models' => $models,
                'jumlah_kamar' => $count,
            ]);
    }
        
}