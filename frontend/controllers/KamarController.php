<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Kamar;
use frontend\models\KamarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\JenisKamar;
use frontend\models\Transaksi;
use common\models\Pelanggan;
use frontend\models\DetailTransaksi;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * KamarController implements the CRUD actions for Kamar model.
 */
class KamarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kamar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KamarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query =  new \yii\db\Query;
        $query->select(['*'])  
        ->from('kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->groupBy('jenis_kamar.tipe_kamar');

        $command = $query->createCommand();
        $kamars = $command->queryAll();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'kamars' =>$kamars,
        ]);
    }

    /**
     * Displays a single Kamar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $bookingForm = new Transaksi();
        $query =  new \yii\db\Query;
        $query->select(['*'])  
        ->from('kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->groupBy('jenis_kamar.tipe_kamar');
        $query->where([
            'jenis_kamar.id' => $id,
            ]);

        $count = (new \yii\db\Query())
        ->from('kamar')
        ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
        ->where(['jenis_kamar.id' => $id])
        ->count();


        $command = $query->createCommand();
        $model = $command->queryOne();

        return $this->render('view', [
            'model' => $model,
            'bookingForm' => $bookingForm,
            'jumlah_kamar' => $count,
        ]);
    }

    public function actionDetailKamar($tipe_kamar,$tanggal_check_in,$tanggal_check_out)
    {
        if($tanggal_check_in > $tanggal_check_out){
            Yii::$app->session->setFlash('danger','maaf tanggal check in harus lebih besar dari tanggal check out');
            return $this->redirect(Url::to(['site/index']));
        }else if($tanggal_check_in < date('Y-m-d')){
            Yii::$app->session->setFlash('danger','maaf tanggal check in sudah lewat');
            return $this->redirect(Url::to(['site/index']));
        }else{
            $bookingForm = new Transaksi();
            $kamar = new Kamar();
            $subQuery = (new \yii\db\Query())
            ->select(['id_kamar'])
            ->from ('transaksi')
            ->innerJoin('detail_transaksi', 'transaksi.id_transaksi = detail_transaksi.id_transaksi')
            ->where(['or',['between','tanggal_check_in',$tanggal_check_in,$tanggal_check_out],
                          ['between','tanggal_check_out',$tanggal_check_in,$tanggal_check_out]]);
            $subQuery->andWhere(['not like', 'status','check out']);

            $query = (new \yii\db\Query())
            ->select(['*'])  
            ->from('kamar')
            ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
            ->where(['and',['id_jenis_kamar' => $tipe_kamar],['not in','id_kamar',$subQuery]]);

            $command = $query->createCommand();
            $model = $command->queryOne();

            $bookingForm->tanggal_check_in = $tanggal_check_in;
            $bookingForm->tanggal_check_out = $tanggal_check_out;

            $count = (new \yii\db\Query())
            ->from('kamar')
            ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
            ->where(['and',['id_jenis_kamar' => $tipe_kamar],['not in','id_kamar',$subQuery]])
            ->count();

            $sql = "SELECT h.harga FROM harga h INNER JOIN jenis_kamar j on h.id_jenis_kamar = j.id WHERE '$tanggal_check_in' BETWEEN h.dari AND h.sampai and h.id_jenis_kamar = ".$tipe_kamar;
            $hargaKamar = Yii::$app->db->createCommand($sql)->queryOne();  

            if($count ==  0)
            {
                Yii::$app->session->setFlash('danger','maaf kamar sudah habis dibooking silahkan booking kamar lain');
            }

            return $this->render('kamar-detail', [
                'model' => $model,
                'bookingForm' => $bookingForm,
                'jumlah_kamar' => $count,
                'kamar' => $kamar,
                'hargaKamar' => $hargaKamar,
            ]);            
        }
    }    

    public function actionBooking()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(Url::to(['site/login']));
        }else{
            $bookingForm = new Transaksi();
            $detail_transaksi = new DetailTransaksi();
            $kamar = new Kamar();
            $user_id = Yii::$app->user->id;
            $pelanggan = Pelanggan::findOne(['account_id' => $user_id]);
            
            if ($bookingForm->load(Yii::$app->request->post()) && $kamar->load(Yii::$app->request->post())) {
                $bookingForm->id_pelanggan = $pelanggan['id_pelanggan'];
                $bookingForm->kode_transaksi = 'SIHSN'.substr(md5(rand()), 0, 10);
                $date1 = date_create($bookingForm->tanggal_check_in);
                $date2 = date_create($bookingForm->tanggal_check_out);
                $diff12 = date_diff($date2, $date1);            
                $bookingForm->durasi = $diff12->d;
                $bookingForm->status = 'menunggu pembayaran';
                $bookingForm->total_pembayaran = $bookingForm->total_pembayaran * $diff12->d * $bookingForm->jumlah_kamar;
                $bookingForm->sisa_pembayaran =  $bookingForm->total_pembayaran;
                $bookingForm->tanggal_booking = date('Y-m-d');
                if($bookingForm->save())
                {
                    $subQuery = (new \yii\db\Query())
                    ->select(['id_kamar'])
                    ->from ('transaksi')
                    ->innerJoin('detail_transaksi', 'transaksi.id_transaksi = detail_transaksi.id_transaksi')
                    ->where(['or',['between','tanggal_check_in',$bookingForm->tanggal_check_in,$bookingForm->tanggal_check_out],
                                  ['between','tanggal_check_out',$bookingForm->tanggal_check_in,$bookingForm->tanggal_check_out]]);
                    $subQuery->andWhere(['not like', 'status','check out']);

                    $query = (new \yii\db\Query())
                    ->select(['*'])  
                    ->from('kamar')
                    ->innerJoin('jenis_kamar', 'kamar.id_jenis_kamar = jenis_kamar.id')
                    ->where(['and',['id_jenis_kamar' => $kamar->id_jenis_kamar],['not in','id_kamar',$subQuery]])
                    ->limit($bookingForm->jumlah_kamar);

                    $command = $query->createCommand();
                    $models = $command->queryAll();
                    foreach ($models as $model) 
                    {
                         $rows[] = [
                                'id_transaksi' => $bookingForm->id_transaksi,
                                'id_kamar' => $model['id_kamar'],
                            ];   
                    }   
                    Yii::$app->db->createCommand()->batchInsert(DetailTransaksi::tableName(),['id_transaksi','id_kamar'], $rows)->execute();

                    $sql = "SELECT h.harga FROM harga h INNER JOIN jenis_kamar j on h.id_jenis_kamar = j.id WHERE '$bookingForm->tanggal_check_in' BETWEEN h.dari AND h.sampai and h.id_jenis_kamar = ".$kamar->id_jenis_kamar;
                    $hargaKamar = Yii::$app->db->createCommand($sql)->queryOne();                 
          
                    Yii::$app->session->setFlash('info','anda berhasil membooking silahkan lakukan pembayaran, masa pembayaran 3 hari dihitung dari saat bookingan berhasil, Untuk melihat No Rekening Pembayaran silahkan memilih menu Bayar di atas dan lampirkan bukti pembayaran');  
                    return $this->render('kamar-detail', [
                        'model' => $model,
                        'bookingForm' => $bookingForm,
                        'jumlah_kamar' => 0,
                        'kamar' => $kamar,
                        'hargaKamar' => $hargaKamar,                    
                    ]);
                }
            }else{
                $this->redirect([Url::to(Url::current())]);          
            }
        }        
    }    

    /**
     * Creates a new Kamar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kamar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kamar]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kamar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kamar]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kamar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kamar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kamar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kamar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
