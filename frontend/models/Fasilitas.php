<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "fasilitas".
 *
 * @property integer $id_fasilitas
 * @property string $nama_fasilitas
 * @property string $deskripsi
 * @property double $harga
 * @property string $gambar
 */
class Fasilitas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fasilitas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_fasilitas', 'deskripsi', 'harga'], 'required'],
            [['harga'], 'number'],
            [['nama_fasilitas'], 'string', 'max' => 60],
            [['deskripsi'], 'string', 'max' => 200],
            [['gambar'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_fasilitas' => 'Id Fasilitas',
            'nama_fasilitas' => 'Nama Fasilitas',
            'deskripsi' => 'Deskripsi',
            'harga' => 'Harga',
            'gambar' => 'Gambar',
        ];
    }
}
