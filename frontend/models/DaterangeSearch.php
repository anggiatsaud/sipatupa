<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Daterange;

/**
 * DaterangeSearch represents the model behind the search form about `frontend\models\Daterange`.
 */
class DaterangeSearch extends Daterange
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDate'], 'integer'],
            [['datetime_start', 'datetime_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Daterange::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idDate' => $this->idDate,
            'datetime_start' => $this->datetime_start,
            'datetime_end' => $this->datetime_end,
        ]);

        return $dataProvider;
    }
}
