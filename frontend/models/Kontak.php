<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kontak".
 *
 * @property integer $id_kontak
 * @property string $email
 * @property string $nama
 * @property string $isi_kontak
 * @property double $ratings
 * @property string $status
 */
class Kontak extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kontak';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ratings'], 'number'],
            [['email', 'nama'], 'string', 'max' => 60],
            [['isi_kontak'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kontak' => 'Id Kontak',
            'email' => 'Email',
            'nama' => 'Nama',
            'isi_kontak' => 'Isi Kontak',
            'ratings' => 'Ratings',
            'status' => 'Status',
        ];
    }
}
