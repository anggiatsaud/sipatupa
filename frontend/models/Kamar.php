<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kamar".
 *
 * @property integer $id_kamar
 * @property integer $no_kamar
 * @property integer $id_jenis_kamar
 *
 * @property JenisKamar $idJenisKamar
 */
class Kamar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kamar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_kamar', 'id_jenis_kamar'], 'integer'],
            [['id_jenis_kamar'], 'required'],
            [['id_jenis_kamar'], 'exist', 'skipOnError' => true, 'targetClass' => JenisKamar::className(), 'targetAttribute' => ['id_jenis_kamar' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kamar' => 'Id Kamar',
            'no_kamar' => 'No Kamar',
            'id_jenis_kamar' => 'Id Jenis Kamar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenisKamar()
    {
        return $this->hasOne(JenisKamar::className(), ['id' => 'id_jenis_kamar']);
    }
}
