<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "daterange".
 *
 * @property integer $idDate
 * @property string $datetime_start
 * @property string $datetime_end
 */
class Daterange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daterange';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDate', 'datetime_start', 'datetime_end'], 'required'],
            [['idDate'], 'integer'],
            [['datetime_start', 'datetime_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDate' => 'Id Date',
            'datetime_start' => 'Datetime Start',
            'datetime_end' => 'Datetime End',
        ];
    }
}
