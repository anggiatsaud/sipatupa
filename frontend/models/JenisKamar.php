<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "jenis_kamar".
 *
 * @property integer $id
 * @property string $tipe_kamar
 * @property string $gambar
 * @property double $harga
 * @property integer $muatan
 * @property string $deskripsi
 *
 * @property Kamar[] $kamars
 */
class JenisKamar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_kamar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['harga', 'muatan', 'deskripsi'], 'required'],
            [['harga'], 'number'],
            [['muatan'], 'integer'],
            [['tipe_kamar', 'gambar'], 'string', 'max' => 64],
            [['deskripsi'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipe_kamar' => 'Tipe Kamar',
            'gambar' => 'Gambar',
            'harga' => 'Harga',
            'muatan' => 'Muatan',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKamars()
    {
        return $this->hasMany(Kamar::className(), ['id_jenis_kamar' => 'id']);
    }
}
