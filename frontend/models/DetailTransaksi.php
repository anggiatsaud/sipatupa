<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "detail_transaksi".
 *
 * @property integer $id
 * @property integer $id_transaksi
 * @property integer $id_kamar
 */
class DetailTransaksi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_transaksi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transaksi', 'id_kamar'], 'required'],
            [['id_transaksi', 'id_kamar'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transaksi' => 'Id Transaksi',
            'id_kamar' => 'Id Kamar',
        ];
    }
}
