<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KamarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kamar';
$this->params['breadcrumbs'][] = $this->title;
$asset         = frontend\assets\AppAsset::register($this);
$baseUrl       =    $asset->baseUrl;
?>
<div class="kamar-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- Filter -->
    

    <!-- Rooms -->
    <section class="rooms mt100">
      <div class="container">
        <div class="row room-list fadeIn appear"> 
          <!-- Room -->
        <?php foreach ($kamars as $kamar){?>          
          <div class="col-sm-4 single">
            <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
              }?>  
            <div class="room-thumb"> <img src="<?=$backend.$kamar['gambar'] ?>" alt="<?= $kamar['id_kamar'] ?>" class="img-responsive" />
              <div class="mask">
                <div class="main">
                  <h5><?=$kamar['tipe_kamar']?></h5>
                  <div class="price">Rp.<?=$kamar['harga']?><span>/malam</span></div>
                </div>
                <div class="content">
                  <p><span>Favorit Room Hotel Serenauli</span> <?=$kamar['deskripsi']?></p>
                  <div class="row">
                    <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i> Air Panas</li>
                        <li><i class="fa fa-check-circle"></i> TV Kabel</li>
                        <li><i class="fa fa-check-circle"></i> AC</li>
                        <li><i class="fa fa-check-circle"></i> Kolam Renang</li>
                         <li><i class="fa fa-check-circle"></i> Karaoke</li>
                         <li><i class="fa fa-check-circle"></i> ATM </li>
                      </ul>
                    </div>
                    <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i> Breakfast </li>
                        
                        <li><i class="fa fa-check-circle"></i> Saluran telepon  </li>
                        
                      
                        
                        <li><i class="fa fa-check-circle"></i> Jogging Track </li>
                      </ul>
                    </div>
                  </div>
                  <?= Html::a('Booking Sekarang', ['kamar/view','id' => $kamar['id']], ['class' => 'btn btn-primary btn-block']) ?> </div>
              </div>
            </div>
          </div>
        <?php } ?>          
        </div>
      </div>
    </section>   
    
</div>
