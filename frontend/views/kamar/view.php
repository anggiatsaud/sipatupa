<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Kamar */

$this->title = $model['tipe_kamar'];
$this->params['breadcrumbs'][] = ['label' => 'Kamar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$asset         = frontend\assets\AppAsset::register($this);
$baseUrl       =    $asset->baseUrl;
?>
<div class="kamar-view">

    

    <div class="container">
      <div class="row"> 
        <!-- Slider -->
        <section class="room-slider standard-slider mt50">
          <div class="col-sm-12 col-md-8">
            <div id="owl-standard" class="owl-carousel">
              <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
                }?>            
              <div class="item"> <img src="<?=$backend.$model['gambar']?>" alt="Bed" class="img-responsive"></a> </div>
            </div>
          </div>
        </section>
        
        <!-- Reservation form -->
        <section id="reservation-form" class="mt50 clearfix">
          <div class="col-sm-12 col-md-4">
            <div class="reservation-vertical clearfix">
              <?php $form = ActiveForm::begin(['action'=>Url::to(['kamar/booking'])]); ?>
              <h2 class="lined-heading"><span>Pemesanan</span></h2>
              <div class="price">
                <h4><?=$model['tipe_kamar']?></h4>
               Rp. <?=$model['harga']?>,-<span> Per Malam</span></div>
              <div id="message"></div>
              <!-- Error message display -->
              <div class="form-group">
                <label for="checkin">Check-in</label>
                <?= DatePicker::widget([
                    'model' => $bookingForm, 
                    'attribute' => 'tanggal_check_in',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'Tanggal Check-in ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]);?>
              </div>
              <div class="form-group"> 
                <label for="checkout">Check-out</label>   
                <?= DatePicker::widget([
                    'model' => $bookingForm, 
                    'attribute' => 'tanggal_check_out',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'Tanggal Check-out ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                ]);?>
              </div>                    
              <div class="form-group">
                  <?= $form->field($bookingForm, 'jumlah_kamar')->dropDownList(range(0, $jumlah_kamar),['prompt'=>'Pilih']) ?>
              </div>
              <div class="price">
                <h4>Jumlah maksimum untuk 1 kamar 2 orang tetapi hotel menyediakan extrabed</div>
              <div id="message"></div>              
              <div class="form-group">             
                <?= Html::a('Booking', ['site/index'], [
                    'class' => 'btn btn-primary btn-block',
                    'data' => [
                        'confirm' => 'Silahkan lakukan search kamar dulu untuk memastikan kamar tersedia',
                        'method' => 'post',
                    ],
                ]) ?>
              </div>
              <?php ActiveForm::end(); ?>  
            </div>
          </div>
        </section>
        
        <!-- Room Content -->
        <section>
          <div class="container">
            <div class="row">
              <div class="col-sm-7 mt50">
                <h2 class="lined-heading"><span>Fasilitas</span></h2>
                <h3 class="mt50"></h3>
                <table class="table table-striped mt30">
                  <tbody>
                    <tr>
                      <td><i class="fa fa-check-circle"></i> Air Panas </td>
                      <td><i class="fa fa-check-circle"></i> TV Kabel</td>
                      <td><i class="fa fa-check-circle"></i> AC </td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-check-circle"></i> Kolam Renang</td>
                      <td><i class="fa fa-check-circle"></i> Karaoke</td>
                      <td><i class="fa fa-check-circle"></i> ATM</td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-check-circle"></i> Breakfast</td>
                      <td><i class="fa fa-check-circle"></i> Saluran Telepon</td>
                      <td><i class="fa fa-check-circle"></i> Jogging Track</td>
                    </tr>
                    <tr>
                      
                      
                    </tr>
                  </tbody>
                </table>
                
              </div>
              <div class="col-sm-5 mt50">
                <h2 class="lined-heading"><span>Overview</span></h2>
                
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                  <li><a href="#facilities" data-toggle="tab">Fasilitas</a></li>
                  <li><a href="#extra" data-toggle="tab">Extra</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="overview">
                    <p>Makanan yang di sediakan sangat lezat dan di hidangkan dengan layak dan berstandar Halal.</p>
                    <p><img src="assets/cb8cb90e/images/tab/restaurant-01.jpg" alt="food" class="pull-right"> Setiap Makanan di Masak oleh Tim Koki yang Handal dan Berprestasi </p>
                  </div>
                  <div class="tab-pane fade" id="facilities">
                    Fasilitas yang diberikan sangat layak , dan sangat menyamankan pengunjung, dan Memberikan Fasilitas Extra Bed 
                  </div>
                  <div class="tab-pane fade" id="extra">Berlaku juga special offer, Ketika ada event yang sedang berlangsung</div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>  
</div>
