<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
     <br>
     <p>Please fill out the following fields to register:</p>
    <div class="row">
         <div class="col-lg-5">
         <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'nama') ?>

            <?= $form->field($model, 'alamat')->textArea(['rows' => '3']) ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'no_telephone') ?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password') ->passwordInput() ?>

            <?= $form->field($model, 'repassword') ->passwordInput() ?>    
            
            <div class="form-group">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
         </div>
     </div>
</div>