<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>
<?= Html::csrfMetaTags() ?>
<div class="site-index">
<!-- Revolution Slider -->
<section class="revolution-slider">
  <div class="bannercontainer">
    <div class="banner">
      <ul>
        <!-- Slide 1 -->
        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" > 
          <!-- Main Image --> 
          <img src="Hotel.jpg" style="opacity:0;" alt="slidebg1"  data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat"> 
          <!-- Layers -->           
          <!-- Layer 1 -->
          <div class="caption sft revolution-starhotel bigtext"  
                        data-x="380" 
                        data-y="30" 
                        data-speed="700" 
                        data-start="1700" 
                        data-easing="easeOutBack"> 
                        <span><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </i></span>Tiara Bunga Hotel <span><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></span></div>
          <!-- Layer 2 -->
          <div class="caption sft revolution-starhotel smalltext"  
                        data-x="315" 
                        data-y="105" 
                        data-speed="800" 
                        data-start="1700" 
                        data-easing="easeOutBack">
                        <span>Kapan lagi bisa menginap di pinggiran Danau Toba ?</span></div>
        <!-- Layer 3 -->
                  <div class="caption sft"  
                        data-x="725" 
                        data-y="175" 
                        data-speed="1000" 
                        data-start="1900" 
                        data-easing="easeOutBack">
                        <a href="index.php?r=kamar%2Findex" class="button btn btn-warning btn-lg">Lihat Kamar</a> 
                  </div>
        </li>
        <!-- Slide 2 -->
        <li data-transition="boxfade" data-slotamount="7" data-masterspeed="1000" > 
          <!-- Main Image --> 
          <img src="slide-bg-02.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat"> 
          <!-- Layers -->           
          <!-- Layer 1 -->
          <div class="caption sft revolution-starhotel bigtext"  
                        data-x="580" 
                        data-y="30" 
                        data-speed="700" 
                        data-start="1700" 
                        data-easing="easeOutBack"> 
                        <span><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </span> BED ROOM SUITE <span><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i ></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></span></div>
          <!-- Layer 2 -->
          <div class="caption sft revolution-starhotel smalltext"  
                        data-x="682" 
                        data-y="105" 
                        data-speed="800" 
                        data-start="1700" 
                        data-easing="easeOutBack">
                        <span>mulai dari Rp.450.000,per malam</span></div>
        <!-- Layer 3 -->
                  <div class="caption sft"  
                        data-x="815" 
                        data-y="175" 
                        data-speed="1000" 
                        data-start="1900" 
                        data-easing="easeOutBack">
                        <a href="index.php?r=kamar%2Fview&id=14" class="button btn btn-warning btn-lg">Pesan kamar ini</a> 
                  </div>
        </li>
      </ul>
    </div>
  </div>
</section>

<!-- Reservation form -->
<section id="reservation-form">
  <div class="container">
    <div class="row">
      <div class="col-md-12">           
        <div class="form-inline reservation-horizontal clearfix">
          <?php $form = ActiveForm::begin(['action'=>Url::to(['site/search'])]); ?>
          <div id="message"></div><!-- Error message display -->
            <div class="row">
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="room">Tipe Kamar</label>
                  <div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."> <i class="fa fa-info-circle fa-lg"> </i> </div>
                    <?= $form->field($model, 'id_jenis_kamar')->dropDownList(
                      ArrayHelper::map($jenis_kamar, 'id', 'tipe_kamar'),
                      ['prompt'=>'Pilih'])->label(false) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="checkin">Check-in</label>
                  <div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Check-In is from 11:00"> <i class="fa fa-info-circle fa-lg"> </i> </div>
                    <?= DatePicker::widget([
                        'model' => $bookingForm, 
                        'attribute' => 'tanggal_check_in',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'options' => ['placeholder' => 'Tanggal Check-in ...'],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format'=>'yyyy-mm-dd'
                        ]
                    ]);?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <label for="checkout">Check-out</label>
                  <div class="popover-icon" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Check-out is from 12:00"> <i class="fa fa-info-circle fa-lg"> </i> </div>
                    <?= DatePicker::widget([
                    'model' => $bookingForm, 
                    'attribute' => 'tanggal_check_out',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'Tanggal Check-out ...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format'=>'yyyy-mm-dd'
                    ]
                  ]);?>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group">             
                  <?= Html::submitButton('Cari',['class' => 'btn btn-primary btn-block', 'name' => 'search-button']) ?>
                </div>
              </div>
            </div>
          <?php ActiveForm::end(); ?>           
        </div>
      </div>  
    </div>
  </div>
</section>
<!-- Rooms -->
<section class="rooms mt50">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="lined-heading"><span>Kamar Favorit </span></h2>
      </div>
      <?php foreach ($kamars as $kamar) { ?>
      <?php                
          $backend = "";
          $path = Url::to("@backend/web/");
          for ($i = 15; $i < strlen($path); $i++) {
            if ($path{$i} === "\\") {
              $backend.= '/';
            } else {
              $backend.= $path{$i};
            }
          }?>  

      <!-- Room -->
      <div class="col-sm-4">
        <div class="room-thumb"> <img src="<?=$backend.$kamar['gambar'] ?>" alt="room 1" class="img-responsive" />
          <div class="mask">
            <div class="main">
              <h5><?=$kamar['tipe_kamar']?></h5>
              <div class="price">Rp.<?=$kamar['harga']?><span>per Malam</span></div>
            </div>
            <div class="content">
              <p><span>Favorit Kamar Hotel Serenauli</span><?=$kamar['deskripsi']?></p>
              <div class="row">
                <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i>Air Panas</li>
                        <li><i class="fa fa-check-circle"></i> TV Kabel</li>
                        <li><i class="fa fa-check-circle"></i> AC</li>
                         <li><i class="fa fa-check-circle"></i> Kolam Renang</li>
                         <li><i class="fa fa-check-circle"></i> Karaoke</li>
                         <li><i class="fa fa-check-circle"></i> ATM </li>
                      </ul>
                    </div>
                    <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i> Breakfast </li>
                        
                        <li><i class="fa fa-check-circle"></i> Saluran telepon  </li>
                        
                      
                        
                        <li><i class="fa fa-check-circle"></i> Jogging Track </li>
                      </ul>
                    </div>
              </div>
              <?= Html::a('Booking Sekarang', ['kamar/view','id' => $kamar['id']], ['class' => 'btn btn-primary btn-block']) ?>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</section>




<!-- USP's -->
<section class="usp mt100">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="lined-heading"></h2>
      </div>
      <div class="col-sm-3 bounceIn appear" data-start="0">
      <div class="box-icon">
        <div class="circle"><i class="fa fa-glass fa-lg"></i></div>
        <h3>Servis Kami</h3>
        <p>Tiara Bunga Hotel & Villa siap memberikan kenyamanan tinggal dengan fasilitas pendukung yang Kami miliki. </p>
        </div>
        </div>
      <div class="col-sm-3 bounceIn appear" data-start="400">
      <div class="box-icon">
        <div class="circle"><i class="fa fa-credit-card fa-lg"></i></div>
        <h3>Pembayaran</h3>
        <p>Dapat membooking dengan membayar 25% dari harga total </p>
        </div>
        </div>
      <div class="col-sm-3 bounceIn appear" data-start="800">
      <div class="box-icon">      
        <div class="circle"><i class="fa fa-cutlery fa-lg"></i></div>
        <h3>24 Jam Restoran</h3>
        <p>Kami menyediakan Restaurant yang menawarkan berbagai cita rasa masakan nasional, "restaurant hotel" satu-satunya yang ada di wilayah Toba Samosir. </p>
        </div>
        </div>
      <div class="col-sm-3 bounceIn appear" data-start="1200">
      <div class="box-icon">
        <div class="circle"><i class="fa fa-phone fa-lg"></i></div>
        <h3>24 Jam Layanan Telepon</h3>
        <p>Kami selalu siap menerima tamu 24 jam, dan siap menjawab semua pertanyaan 
anda mengenai hotel Kami.</p>
        </div>
    </div>
    </div>
  </div>
</section>

</div>
