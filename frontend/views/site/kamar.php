<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\KamarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kamars';
$this->params['breadcrumbs'][] = $this->title;
$asset         = frontend\assets\AppAsset::register($this);
$baseUrl       =    $asset->baseUrl;
?>
<div class="kamar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!--  
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <ul class="nav nav-pills" id="filters">
            <li class="active"><a href="#" data-filter="*">All</a></li>
            <li><a href="#" data-filter=".single">Single Room</a></li>
            <li><a href="#" data-filter=".double">Double Room</a></li>
            <li><a href="#" data-filter=".executive">Executive Room</a></li>
            <li><a href="#" data-filter=".apartment">Apartment</a></li>
          </ul>
        </div>
      </div>
    </div> -->

    <!-- Rooms -->
    <section class="rooms mt100">
      <div class="container">
        <div class="row room-list fadeIn appear"> 
          <!-- Room -->
        <?php foreach ($kamars as $kamar){?>          
          <div class="col-sm-4 single">
            <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
              }?>  
            <div class="room-thumb"> <img src="<?=$backend.$kamar['id_kamar'] ?>" alt="<?= $kamar->id_kamar ?>" class="img-responsive" />
              <div class="mask">
                <div class="main">
                  <h5>Double bedroom</h5>
                  <div class="price"><?=$kamar->harga?><span>/malam</span></div>
                </div>
                <div class="content">
                  <p><span>A modern hotel room in Star Hotel</span> Nunc tempor erat in magna pulvinar fermentum. Pellentesque scelerisque at leo nec vestibulum. 
                    malesuada metus.</p>
                  <div class="row">
                    <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                        <li><i class="fa fa-check-circle"></i> Private balcony</li>
                        <li><i class="fa fa-check-circle"></i> Sea view</li>
                      </ul>
                    </div>
                    <div class="col-xs-6">
                      <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle"></i> Free Wi-Fi</li>
                        <li><i class="fa fa-check-circle"></i> Incl. breakfast</li>
                        <li><i class="fa fa-check-circle"></i> Bathroom</li>
                      </ul>
                    </div>
                  </div>
                  <?= Html::a('Book Now', ['kamar/view','id'=>$kamar->id_kamar], ['class' => 'btn btn-primary btn-block']) ?> </div>
              </div>
            </div>
          </div>
        <?php } ?>          
        </div>
      </div>
    </section>   
</div>
