
<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;

$coord = new LatLng(['lat' => 2.3526499, 'lng' => 99.12212320000003]);
$map = new Map([
    'center' => $coord,
    'zoom' => 200,
        ]);
// lets use the directions renderer
//$lokasi1 = new LatLng(['lat' => 2.3526499, 'lng' => 99.12212320000003]);
$lokasi1 = new LatLng(['lat' => 2.3526499, 'lng' => 99.12212320000003]);
//$lokasi2 = new LatLng(['lat' => 2.3548136, 'lng' => 99.12697189999994]);
$lokasi3 = new LatLng(['lat' => 2.35442, 'lng' => 99.126917]);
//$lokasi3 = new LatLng(['lat' => 2.3514768, 'lng' => 99.11711489999993]);
$lokasi2 = new LatLng(['lat' => 2.3514768, 'lng' => 99.11711489999993]);

// setup just one waypoint (Google allows a max of 8)
$waypoints = [
    new DirectionsWayPoint(['location' => $lokasi3])
];

$directionsRequest = new DirectionsRequest([
    'origin' => $lokasi2,
    'destination' => $lokasi1,
    'waypoints' => $waypoints,
    'travelMode' => TravelMode::WALKING
        ]);

// Lets configure the polyline that renders the direction
$polylineOptions = new PolylineOptions([
    'strokeColor' => '#FFAA00',
    'draggable' => true
        ]);

// Now the renderer
$directionsRenderer = new DirectionsRenderer([
    'map' => $map->getName(),
    'polylineOptions' => $polylineOptions
        ]);

// Finally the directions service
$directionsService = new DirectionsService([
    'directionsRenderer' => $directionsRenderer,
    'directionsRequest' => $directionsRequest
        ]);

// Thats it, append the resulting script to the map
$map->appendScript($directionsService->getJs());

// Lets add a marker now
$marker = new Marker([
    'position' => $lokasi1,
    'title' => 'Hotel Sere Nauli',
        ]);

// Provide a shared InfoWindow to the marker
$marker->attachInfoWindow(
        new InfoWindow([
    'content' => '<p>Lokasi Hotel Sere Nauli'
    . '<br>Laguboti, Sumatera Utara</p>'
        ])
);

// Add marker to the map
$map->addOverlay($marker);

// Now lets write a polygon
$coords = [
    new LatLng(['lat' => 2.350823799999999, 'lng' => -99.1147211]),
//    new LatLng(['lat' => 18.466465, 'lng' => -66.118292]),
//    new LatLng(['lat' => 32.321384, 'lng' => -64.75737]),
//    new LatLng(['lat' => 25.774252, 'lng' => -80.190262])
];

$polygon = new Polygon([
    'paths' => $coords
        ]);

// Add a shared info window
$polygon->attachInfoWindow(new InfoWindow([
    'content' => '<p>This is my super cool Polygon</p>'
]));

// Add it now to the map
$map->addOverlay($polygon);


// Lets show the BicyclingLayer :)
$bikeLayer = new BicyclingLayer(['map' => $map->getName()]);

// Append its resulting script
$map->appendScript($bikeLayer->getJs());

// Display the map -finally :)
echo $map->display();
?>
