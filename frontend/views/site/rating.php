<?php

    use kartik\widgets\StarRating;
    use yii\web\JsExpression;
     
    // Fractional star rating with 6 stars , custom star symbol (uses glyphicon heart),
    // custom captions, and customizable ranges.
    echo StarRating::widget(['name' => 'rating_19',
    'pluginOptions' => [
    'stars' => 6,
    'min' => 0,
    'max' => 6,
    'step' => 0.1,
    'filledStar' => '<i class="glyphicon glyphicon-heart"></i>',
    'emptyStar' => '<i class="glyphicon glyphicon-heart-empty"></i>',
    'defaultCaption' => '{rating} hearts',
    'starCaptions' => new JsExpression("function(val){return val == 1 ? 'One heart' : val + ' hearts';}")
    ]
    ]);
     
    // Set size of the control to extra large and reduce number of stars
    echo StarRating::widget([
    'name' => 'rating_20',
    'pluginOptions' => [
    'size' => 'xl',
    'stars' => 3,
    'min' => 0,
    'max' => 3
    ],
    ]);
     
    // Set a preset value, make it readonly, hide caption, and hide clear button.
    echo StarRating::widget([
    'name' => 'rating_21',
    'value' => 2,
    'pluginOptions' => [
    'readonly' => true,
    'showClear' => false,
    'showCaption' => false,
    ],
    ]);
     
    // Control the number of stars, each caption value, and styles for display
    echo StarRating::widget([
    'name' => 'rating_21',
    'pluginOptions' => [
    'min' => 0,
    'max' => 12,
    'step' => 2,
    'size' => 'lg',
    'starCaptions' => [
    0 => 'Extremely Poor',
    2 => 'Very Poor',
    4 => 'Poor',
    6 => 'Ok',
    8 => 'Good',
    10 => 'Very Good',
    12 => 'Extremely Good',
    ],
    'starCaptionClasses' => [
    0 => 'text-danger',
    2 => 'text-danger',
    4 => 'text-warning',
    6 => 'text-info',
    8 => 'text-primary',
    10 => 'text-success',
    12 => 'text-success'
    ],
    ],
    ]);
     
    // Advanced theming and ability to use richer markup (e.g. SVG).
    // Use the inbuilt Krajee SVG theme for rendering SVG icons
    echo StarRating::widget(['name' => 'rating_44',
    'pluginOptions' => [
    'theme' => 'krajee-svg',
    'filledStar' => '<span class="krajee-icon krajee-icon-star"></span>',
    'emptyStar' => '<span class="krajee-icon krajee-icon-star"></span>'
    ]
    ]);