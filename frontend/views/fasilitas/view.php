<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Fasilitas */

$this->title = $model->nama_fasilitas;
$this->params['breadcrumbs'][] = ['label' => 'Fasilitas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fasilitas-view">


    <div class="container">
      <div class="row"> 

        <!-- Slider -->
        <section class="room-slider standard-slider mt50">
          <div class="col-sm-12 col-md-8">
            <div id="owl-standard" class="owl-carousel">
              <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
                }?>            
              <div class="item"> <a href="<?=$backend.$model['gambar']?>" data-rel="prettyPhoto[gallery1]"><img src="<?=$backend.$model['gambar']?>" alt="Bed" class="img-responsive"></a> </div>
            </div>
          </div>
        </section>      
    
        <!-- Reservation form -->
        <section id="reservation-form" class="mt50 clearfix">
          <div class="col-sm-12 col-md-4">
            <div class="reservation-vertical clearfix">
              <h2 class="lined-heading"><span>Pemesanan</span></h2>
              <div class="price">
                <h4><?=$model->nama_fasilitas?></h4><br>
                <span>Pemesanan dapat menghubungi Costumer Kami </span></div>
              <div id="message"></div>
              <!-- Error message display -->  
            </div>
          </div>
        </section>
        <!-- Room Content -->
        <section>
          <div class="container">
            <div class="row">
              <div class="col-sm-7 mt50">
                <h2 class="lined-heading"><span>Detail Fasilitas</span></h2>
                <?=$model['deskripsi']?>
                
                
              </div>
              <div class="col-sm-5 mt50">
                <h2 class="lined-heading"><span>Overview</span></h2>
                
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#overview" data-toggle="tab">Overview</a></li>
                  
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="overview">
                    <p>Fasilitas yang berikan sangat bermanfaat bagi Pengunjung</p>
                    
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>  

</div>
