<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\FasilitasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fasilitas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_fasilitas') ?>

    <?= $form->field($model, 'nama_fasilitas') ?>

    <?= $form->field($model, 'deskripsi') ?>

    <?= $form->field($model, 'harga') ?>

    <?= $form->field($model, 'gambar') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
