<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\FasilitasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fasilitas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fasilitas-index">


<!-- Filter -->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          
        </div>
      </div>
    </div>

    <!-- Rooms -->
    <section class="rooms mt100">
      <div class="container">
        <div class="row room-list fadeIn appear"> 
          <!-- Room -->
        <?php foreach ($fasilitass as $fasilitas){?>          
          <div class="col-sm-4 single">
            <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
              }?>  
            <div class="room-thumb"> <img src="<?=$backend.$fasilitas->gambar ?>" alt="<?= $fasilitas->id_fasilitas ?>" class="img-responsive" />
              <div class="mask">
                <div class="main">
                  <h5><?=$fasilitas->nama_fasilitas?></h5>
                  
                </div>
                <div class="content">
                  <p><span>Fasilitas di Tiara Bunga Hotel & Vila</span> <?=$fasilitas->deskripsi?>
                  <div class="row">
                    
                    
                  </div>
                  <?= Html::a('Selengkapnya', ['fasilitas/view','id' => $fasilitas->id_fasilitas], ['class' => 'btn btn-primary btn-block']) ?> </div>
              </div>
            </div>
          </div>
        <?php } ?>          
        </div>
      </div>
    </section>   
</div>
