<?php
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\rating\StarRating;


/* @var $this yii\web\View */
/* @var $model frontend\models\Kontak */

$this->title = 'Feedback';

$this->params['breadcrumbs'][] = $this->title;
$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>
<div class="kontak-create">

    
<div class="container">
  <div class="row"> 
    
    <!-- Contact details -->
    <section class="contact-details">
      <div class="col-md-5">
        <h2 class="lined-heading  mt50"><span>Alamat</span></h2>
        <!-- Panel -->
        <div class="panel panel-default text-center">
          <div class="panel-heading">
            <div class="panel-title"></i> <strong>Tiara Bunga Hotel & Villa</strong></div>
          </div>
          <div class="panel-body">
          <div class="row">
            <div class="col-xs-4">
              <div class="box-icon"> <a href="https://www.facebook.com/SERE-NAULI-HOTEL-Laguboti-788180347894874/" target="blank">
                <div class="circle"><i class="fa fa-facebook fa-lg"></i></div>
                </a> </div>
            </div>
            <div class="col-xs-4">
              <div class="box-icon"> <a href="https://twitter.com/search?q=hotel%20serenauli&src=typd">
                <div class="circle"><i class="fa fa-twitter fa-lg"></i></div>
                </a> </div>
            </div>
            <div class="col-xs-4">
              <div class="box-icon"> <a href="https://www.instagram.com/explore/tags/serenauli/">
                <div class="circle"><i class="fa fa-instagram fa-lg"></i></div>
                </a> </div>
            </div>
          </div>
          </div>
        </div>
        <!-- GMap -->
                          <?php
                    $coord = new LatLng(['lat' => 2.3526499, 'lng' => 99.12212320000003]);
                    $map = new Map([
                        'center' => $coord,
                        'zoom' => 250,
                    ]);
                    // lets use the directions renderer
                    //$lokasi1 = new LatLng(['lat' => 2.3526499, 'lng' => 99.12212320000003]);
                    $lokasi1 = new LatLng(['lat' => 2.3526499, 'lng' => 99.12212320000003]);
                    //$lokasi2 = new LatLng(['lat' => 2.3548136, 'lng' => 99.12697189999994]);
                    $lokasi3 = new LatLng(['lat' => 2.35442, 'lng' => 99.126917]);
                    //$lokasi3 = new LatLng(['lat' => 2.3514768, 'lng' => 99.11711489999993]);
                    $lokasi2 = new LatLng(['lat' => 2.3514768, 'lng' => 99.11711489999993]);

                    // setup just one waypoint (Google allows a max of 8)
                    $waypoints = [
                        new DirectionsWayPoint(['location' => $lokasi3])
                    ];

                    $directionsRequest = new DirectionsRequest([
                        'origin' => $lokasi2,
                        'destination' => $lokasi1,
                        'waypoints' => $waypoints,
                        'travelMode' => TravelMode::WALKING
                    ]);

                    // Lets configure the polyline that renders the direction
                    $polylineOptions = new PolylineOptions([
                        'strokeColor' => '#FFAA00',
                        'draggable' => true
                    ]);

                    // Now the renderer
                    $directionsRenderer = new DirectionsRenderer([
                        'map' => $map->getName(),
                        'polylineOptions' => $polylineOptions
                    ]);

                    // Finally the directions service
                    $directionsService = new DirectionsService([
                        'directionsRenderer' => $directionsRenderer,
                        'directionsRequest' => $directionsRequest
                    ]);

                    // Thats it, append the resulting script to the map
                    $map->appendScript($directionsService->getJs());

                    // Lets add a marker now
                    $marker = new Marker([
                        'position' => $lokasi1,
                        'title' => 'Hotel Sere Nauli',
                    ]);

                    // Provide a shared InfoWindow to the marker
                    $marker->attachInfoWindow(
                            new InfoWindow([
                        'content' => '<p>Lokasi Hotel Sere Nauli'
                        . '<br>Laguboti, Sumatera Utara</p>'
                            ])
                    );

                    // Add marker to the map
                    $map->addOverlay($marker);

                    // Now lets write a polygon
                    $coords = [
                        new LatLng(['lat' => 2.350823799999999, 'lng' => -99.1147211]),
                            //    new LatLng(['lat' => 18.466465, 'lng' => -66.118292]),
                            //    new LatLng(['lat' => 32.321384, 'lng' => -64.75737]),
                            //    new LatLng(['lat' => 25.774252, 'lng' => -80.190262])
                    ];

                    $polygon = new Polygon([
                        'paths' => $coords
                    ]);

                    // Add a shared info window
                    $polygon->attachInfoWindow(new InfoWindow([
                        'content' => '<p>This is my super cool Polygon</p>'
                    ]));

                    // Add it now to the map
                    $map->addOverlay($polygon);


                    // Lets show the BicyclingLayer :)
                    $bikeLayer = new BicyclingLayer(['map' => $map->getName()]);

                    // Append its resulting script
                    $map->appendScript($bikeLayer->getJs());

                    // Display the map -finally :)
                    echo $map->display();
                    ?>

        
      </div>
    </section>
    
    <!-- Contact form -->
    <section id="contact-form" class="mt50">
      <div class="col-md-7">
        <h2 class="lined-heading"><span>Kirim Pesan</span></h2>
        <div id="message"></div>
        <!-- Error message display -->
          <div class="row">
           <?php $form = ActiveForm::begin(['action'=>Url::to(['kontak/create'])]); ?>
            <div class="col-md-6">
              <div class="form-group">
                <label for="name" accesskey="U"><span class="required">*</span> Rating </label>
                <?= $form->field($model, 'ratings')->widget(StarRating::classname(), [
                    'pluginOptions' => ['size'=>'xs']
                ])->label(false);?>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="email" accesskey="E"><span class="required">*</span> Rating Hotel</label>
                    <input class="rating" value="<?=$rating;?>" type="number" showclear="false" showcaption="false" data-size="xs" data-readonly="true" min="0" max="5" step="0.1"/>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="nama" accesskey="C"><span class="required">*</span>Nama</label>
            <?= $form->field($model, 'nama')->textInput(['maxlength' => true])->label(false) ?>
          </div>          
          <div class="form-group">
            <label for="nama" accesskey="C"><span class="required">*</span> Email</label>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label(false) ?>
          </div>            
          <div class="form-group">
            <label for="comments" accesskey="C"><span class="required">*</span> Pesan </label>
            <?= $form->field($model, 'isi_kontak')->textInput(['maxlength' => true])->textArea(['rows' => '11'])->label(false) ?>
          </div>
          <div class="form-group">             
            <?= Html::submitButton('Kirim',['class' => 'btn btn-primary btn-block', 'name' => 'kontak-button']) ?>
          </div>
         <?php ActiveForm::end(); ?>
      </div>
    </section> 
  </div>
</div>
<div class="container">
  <div class="row"> 
  <?php
    $m = 0;
    $namas = array();
    $isi_kontaks = array();
    $ratingPelanggan = array();
    foreach ($kontaks as $kontak) {
      $namas[] = $kontak->nama;
      $isi_kontaks[] = $kontak->isi_kontak;
      $ratingPelanggan[] = $kontak->ratings;
      $m++;
    }
  ?>   
    <?php                
        $backend = "";
        $path = Url::to("@backend/web/images/uploads");
        for ($i = 15; $i < strlen($path); $i++) {
            if ($path{$i} === "\\") {
                $backend.= '/';
            } else {
                $backend.= $path{$i};
            }
    }?>  
    <!-- Testimonials -->
    <section class="testimonials mt50">
      <div class="col-md-12 col-sm-12">
        <h3 class="lined-heading"><span>Komentar</span></h3>
        <div id="owl-reviews" class="owl-carousel mt30">
          <div class="item">
          <?php for($k=0;$k<$m;$k++) { ?>
            <div class="row">
              <div class="col-lg-2 col-md-4 col-sm-2 col-xs-12"> <img src="<?=$backend?>/review-01.png" alt="Review 1" class="img-circle" /></div>
                  <input class="rating" value="<?=$ratingPelanggan[$k]?>" type="number" showclear="false" showcaption="false" data-size="xs" data-readonly="true" min="0" max="5" step="0.1"/>
              <div class="col-lg-10 col-md-8 col-sm-10 col-xs-12">
                <div class="text-balloon">
                  <?=$isi_kontaks[$k]?><span><?=$namas[$k]?>
                </div>
              </div>
            </div>
            <?php } ?> 
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

</div>
