<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;


//AppAsset::register($this);
$asset         = frontend\assets\AppAsset::register($this);
$baseUrl       =    $asset->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-50960990-1', 'slashdown.nl');
      ga('send', 'pageview');
    </script>    
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <!-- Top header -->
    <div id="top-header">
      <div class="container">
        <div class="row">
          <div class="col-xs-6">
            <div class="th-text pull-left">
              <div class="th-item"> <i class="fa fa-phone"></i> 0821-6242-5702 </div>
              <div class="th-item"> <i class="fa fa-envelope"></i> tiara@yahoo.com </div>
            </div>
          </div>
          <div class="col-xs-6">
            <div class="th-text pull-right">
              <div class="th-item">
               
              </div>
              <div class="th-item">
                <div class="social-icons"> <a href="https://www.facebook.com/SERE-NAULI-HOTEL-Laguboti-788180347894874/"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/watch?v=Kp75-XOLIEM"><i class="fa fa-youtube-play"></i></a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="wrap">
    <!-- Header -->
    <header>
      <!-- Navigation -->
      <div class="navbar yamm navbar-default" id="sticky">
        <div class="container">      
              <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand">         
                <!-- Logo -->
                 <a href="#" data-filter=".single"><h1>TIARA BUNGA HOTEL & VILLA</h1></a>
          <div id="navbar-collapse-grid" class="navbar-collapse collapse">
                <?php
                $menuItems = [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label'=>'Kamar', 'url'=>['/kamar/index']],
                    ['label' => 'Fasilitas', 'url' =>['/fasilitas/index']],  
                    ['label' => 'Bayar', 'url'=>['/transaksi/index']],
                    ['label' => 'Tentang', 'url' => ['/site/about']],
                    ['label' => 'Feedback', 'url' => ['/kontak/create']], 
                    ['label' => 'Harga Promosi','url' =>['harga/index']],        
                ];
                if (Yii::$app->user->isGuest) {                                 
                    $menuItems[] = ['label' => 'Register', 'url' => ['/site/register']];
                    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
                } else {
                    $menuItems[] = [
                        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ];                    
                }
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => $menuItems,
                ]);
                ?>
          </div>
        </div>
      </div>
    </header>

    <?php $i = Url::current();?>
    <?php if($i == Url::to("@web/index.php?r=site%2Findex")) { ?>
    <div> 
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>    
    <?php } else { ?>   
        <!-- Parallax Effect -->
       <!--  <script type="text/javascript">$(document).ready(function(){$('#parallax-pagetitle').parallax("50%", -0.55);});</script>

        <section class="parallax-effect">
          <div id="parallax-pagetitle" style="background-image: url(assets/cb8cb90e/images/parallax/parallax-01.jpg);">
            <div class="color-overlay">  -->
              <!-- Page title -->
            <!--   <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <h1><?= Html::encode($this->title) ?></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>       -->
        <div class="container">            
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>      
    <?php }?>
</div>

<!-- Footer -->
<footer>
<div class="container">
  <div class="row">
    <div class="col-md-3 col-sm-3">
      <h4>Tentang Tiara Bunga Hotel & Villa</h4>
      <p>This property offers an airport shuttle, non-smoking rooms and free WiFi. Facing the seafront in Balige, Tiara Bunga Hotel & Villa features an outdoor pool and a private beach area. Featuring a garden, the 3-star resort has air-conditioned rooms with a private bathroom..<br>
    </div>
    <div class="col-md-3 col-sm-3">
      <h4>Event</h4>
      <p>Kami Akan Menberitahu setiap event yg kami berikan</p>
      <li><p>Discount 5% Ramadhan</p></li>
      
    </div>
    <div class="col-md-3 col-sm-3">
      <h4>Blog Kami</h4>
        <ul>    
        <li>
      <article>
      <a href="http://www.serenaulihotel.blogspot.com">tiara.blogspot.com</a>
      </article>
    </li>
        <li>
      <article>
      <a href="http://serenaulihotellaguboti.blogspot.com">tiara.blogspot.com</a>
      </article>
    </li>
       <article></article>
        </ul>
    </div>
    <div class="col-md-3 col-sm-3">
      <h4>Alamat</h4>
      <address>
      <strong>Tiara Bunga Hotel & Villa</strong><br>
       Jl. Tuktuk Tarabunga, Tara Bunga<br>
      Tampahan-Toba Samosir<br>
      Sumatera Utara - Indonesia<br>
      <abbr title="Phone">Phone:</abbr> 0821-6242-5702<br>
      <abbr title="Email">Email:</abbr> tiara@yahoo.com<br>
 
      
      </address>
    </div>
  </div>
</div>
<div class="footer-bottom">
<div class="container">
<div class="row">
<div class="col-xs-6"> &copy  All Rights Reserved By TYP03 2018</div>
<div class="col-xs-6 text-right">
<ul>
<li></li>
</ul>
</div>
</div>
</div>
</div>
</footer>

<!-- Go-top Button -->
<div id="go-top"><i class="fa fa-angle-up fa-2x"></i></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
