<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;
/* @var $this yii\web\View */
/* @var $model frontend\models\Rating */
/* @var $form yii\widgets\ActiveForm */
$asset = frontend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>

<div class="rating-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rating')->widget(StarRating::classname(), [
    	'pluginOptions' => ['size'=>'lg']
		]);
	?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?> 
    <input class="rating" value="<?=$rating;?>" type="number" showclear="false" disabled="true" />
    <input class="rating" value="<?=$rating;?>" type="number" showclear="false" showcaption="false" data-size="xs" data-readonly="true" min="0" max="5" step="0.1"/>
</div>
