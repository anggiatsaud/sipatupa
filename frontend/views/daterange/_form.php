<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Daterange */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daterange-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idDate')->textInput() ?>

    <?= $form->field($model, 'datetime_start')->textInput() ?>

    <?= $form->field($model, 'datetime_end')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
