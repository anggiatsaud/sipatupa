<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DaterangeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dateranges';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daterange-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Daterange', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idDate',
            'datetime_start',
            'datetime_end',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
