<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Daterange */

$this->title = 'Create Daterange';
$this->params['breadcrumbs'][] = ['label' => 'Dateranges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daterange-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
