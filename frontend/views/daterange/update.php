<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Daterange */

$this->title = 'Update Daterange: ' . $model->idDate;
$this->params['breadcrumbs'][] = ['label' => 'Dateranges', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idDate, 'url' => ['view', 'id' => $model->idDate]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daterange-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
