<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Pembayaran */

$this->title = 'Pembayaran';
$this->params['breadcrumbs'][] = ['label' => 'Transaksi', 'url' => ['transaksi/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pembayaran-create">

  

    <?= $this->render('_form', [
        'model' => $model,
        'harus_dibayar' => $harus_dibayar,
    ]) ?>
    <?=$harus_dibayar?>

</div>
