<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Pembayaran */

$this->title = 'Update Pembayaran: ' . $model->no_rekening;
$this->params['breadcrumbs'][] = ['label' => 'Transaksi', 'url' => ['transaksi/index']];
$this->params['breadcrumbs'][] = ['label' => 'Bukti Pembayaran', 'url' => ['view', 'id' => $model->id_pembayaran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pembayaran-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
