<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model frontend\models\Pembayaran */

$this->title = 'Bukti Pembayaran';
$this->params['breadcrumbs'][] = ['label' => 'Transaksi', 'url' => ['transaksi/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pembayaran-view">

    

    
<br>
    <div class="col-md-12">
        <div class="col-md-3">
            <?php                
                $backend = "";
                $path = Url::to("@backend/web/");
                for ($i = 15; $i < strlen($path); $i++) {
                    if ($path{$i} === "\\") {
                        $backend.= '/';
                    } else {
                        $backend.= $path{$i};
                    }
              }?> 
            <img src="<?=$backend.$model->bukti_pembayaran?>" height="225"  width="225">
        </div>
        <div class="col-md-9">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id_pembayaran',
                    //'id_transaksi',
                    //'id_pelanggan',
                    //'id_staff',
                    'no_rekening',
                    'total_transaksi',
                    //'bukti_pembayaran',
                ],
            ]) ?> 
            
        </div>
    </div>    


</div>
